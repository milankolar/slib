#########################################################################
#
#  SLiBBrowserPy.py Pro v1.42 by DGDM
#
#########################################################################

import maya.cmds as cmds
import maya.mel as mel
import time
import os
import shutil
import sys
import platform
import webbrowser
import re
import maya.OpenMaya as om
import maya.OpenMayaUI as omui
from functools import partial
import math as math
import subprocess

MAYA_VERSION = cmds.about(version=1)

if MAYA_VERSION != '2017':
    from PySide import QtGui, QtCore
    from PySide.QtGui import QMainWindow
    from PySide.QtGui import QWidget
    from PySide.QtGui import QResizeEvent
    import shiboken
else:
    from PySide2 import QtGui, QtCore, QtWidgets
    from PySide2.QtCore import *
    from PySide2.QtWidgets import QMainWindow
    from PySide2.QtWidgets import QWidget
    from PySide2.QtGui import QResizeEvent
    import shiboken2
    QtGui.QDialog = QtWidgets.QDialog
    QtGui.QWidget = QtWidgets.QWidget
    from shiboken2 import wrapInstance
    shiboken = shiboken2

    WorkspaceName = "WorkspaceBrowser"

SLiBImage = mel.eval('getenv SLiBImage;')
SLiBGuiPath = mel.eval('getenv SLiBGui;')
SLiBTempStore = mel.eval('getenv SLiBLib;') + "/scene/"

def getRenderEngine():
    return cmds.getAttr('defaultRenderGlobals.currentRenderer')

#IMPORT
def SLiBBrowserImport(mode):
    allMats = cmds.ls(mat=1)
    if cmds.iconTextCheckBox('ipt', q=1, v=1) != 1:
        sel = cmds.ls(sl=1, fl=1)
    multiSel = SLiBMultiSel()
    if getRenderEngine() == 'redshift':
        cmds.rsRender( r=1, stopIpr=1)

    if mode == 'Normal':
        for e in multiSel:
            cmds.iconTextRadioButton('icon' + e, e=1, sl=1)

            if gib('mainCat') == 'shader':
                if cmds.menuItem('importREF', q=1, cb=1) != True:
                    imported = cmds.file(gib('file'), i=1, type=SLiBFileType(), uns=0, rnn=1, iv=1)
                else:
                    imported = cmds.file(gib('file'), r=1, type=SLiBFileType(), uns=1, rnn=1, iv=1, ns=gib('name'))

                importedMats = cmds.ls(imported, mat=1)
                importedShapes = cmds.ls(imported, s=1)
                root = cmds.ls(imported, assemblies=1)

                SLiBShaderCheck(sel, root, importedShapes, importedMats, allMats)
                SLiBMessager('IMPORT successful!', 'green')

            if gib('mainCat') == 'objects':
                ImpMode = SLiBHistoryCheck()

                if ImpMode == 'Import':
                    if cmds.menuItem('importREF', q=1, cb=1) != True:
                        imported = cmds.file(gib('file'), i=1, type=SLiBFileType(), uns=0, rnn=1, iv=1)
                    else:
                        imported = cmds.file(gib('file'), r=1, type=SLiBFileType(), uns=1, rnn=1, iv=1, ns=gib('name'))

                    importedMats = cmds.ls(imported, mat=1)
                    importedShapes = cmds.ls(imported, s=1)
                    root = cmds.ls(imported, assemblies=1)

                    i=1
                    while cmds.objExists(root[0] + str(i).zfill(3)):
                        i+=1
                    root = cmds.rename(root[0], root[0] + '_' + str(i).zfill(3))

                    SLiBAddRoot(root)

                    SLiBMessager('IMPORT successful!', 'green')
                    if cmds.iconTextCheckBox('ipt', q=1, v=1) != 1:
                        cmds.select(root)
                    return root

                if ImpMode == 'Clone':
                    if cmds.menuItem('ReUseDuplicate', q=1, rb=1) == 1:
                        duplObject = cmds.duplicate(rootClone, n=str(rootClone)+'_Dupl_001')
                        SLiBMessager('DUPLICATED!', 'green')
                        cmds.move(duplObject, rpr=1)
                        if cmds.iconTextCheckBox('ipt', q=1, v=1) != 1:
                            cmds.select(duplObject)
                        return duplObject

                        #SLiBAddRoot(duplObject)

                    if cmds.menuItem('ReUseInstance', q=1, rb=1) == 1:
                        instObject = cmds.instance(rootClone, n=str(rootClone)+'_Inst_001')
                        SLiBMessager('INSTANCED!', 'green')
                        cmds.move(instObject, rpr=1)
                        if cmds.iconTextCheckBox('ipt', q=1, v=1) != 1:
                            cmds.select(instObject)
                        return instObject

                        #SLiBAddRoot(instObject)

    else:
        if len(sel) != 0:
            #if mode == 'Replace':
            #    selSG = cmds.listConnections(cmds.ls(sel, o=1, dag=1, s=1), type = "shadingEngine")
            #    for l in list(set(selSG)):
            #        if l != 'initialShadingGroup' and l != 'lambert1':
            #            cmds.select(cmds.listHistory(l), noExpand=1)
            #            cmds.delete()

            for v in sel:
              for e in multiSel:

                cmds.iconTextRadioButton('icon' + e, e=1, sl=1)

                if gib('mainCat') == 'objects':
                    ImpMode = SLiBHistoryCheck()

                    if ImpMode == 'Import':
                        if cmds.menuItem('importREF', q=1, cb=1) != True:
                            imported = cmds.file(gib('file'), i=1, type=SLiBFileType(), uns=0, rnn=1, iv=1)
                        else:
                            imported = cmds.file(gib('file'), r=1, type=SLiBFileType(), uns=1, rnn=1, iv=1)

                        importedMats = cmds.ls(imported, mat=1)
                        importedShapes = cmds.ls(imported, s=1)
                        root = cmds.ls(imported, assemblies=1)

                        i=1
                        while cmds.objExists(root[0] + str(i).zfill(3)):
                            i+=1
                        root = cmds.rename(root[0], root[0] + '_' + str(i).zfill(3))

                        SLiBAddRoot(root)

                        SLiBMessager('IMPORT successful!', 'green')

                    if ImpMode == 'Clone':
                        if cmds.menuItem('ReUseDuplicate', q=1, rb=1) == 1:
                            duplObject = cmds.duplicate(rootClone, n=str(rootClone)+'_Dupl_001')
                            SLiBMessager('DUPLICATED!', 'green')

                            #SLiBAddRoot(duplObject)

                        if cmds.menuItem('ReUseInstance', q=1, rb=1) == 1:
                            instObject = cmds.instance(rootClone, n=str(rootClone)+'_Inst_001')
                            SLiBMessager('INSTANCED!', 'green')

                            #SLiBAddRoot(instObject)

                        root = rootClone

                    curPosition = cmds.xform(v, q=1, t=1, ws=1)
                    curRotation = cmds.xform(v, q=1, ro=1, ws=1)

                    cmds.setAttr(root + '.tx', curPosition[0])
                    cmds.setAttr(root + '.ty', curPosition[1])
                    cmds.setAttr(root + '.tz', curPosition[2])
                    cmds.setAttr(root + '.rx', curRotation[0])
                    cmds.setAttr(root + '.ry', curRotation[1])
                    cmds.setAttr(root + '.rz', curRotation[2])

                    if mode == 'Replace':
                        cmds.delete(v)

            if mode == 'Place':
                cmds.select(sel)
                if cmds.ls(sel, hl=1) == 1:
                    cmds.selectMode(co=1)
                SLiBMessager(str(len(sel)) + ' Objects imported and placed!' , 'green')

            if mode == 'Replace':
                cmds.select(root)
                SLiBMessager(str(len(sel)) + ' Objects replaced!' , 'green')

        else:
            SLiBMessager('Please select target Object(s) or Verticies!', 'red')
            sys.exit()

    if cmds.window("SLiBvprWindow", ex=1) == 1:
        cmds.rsRender( r=1, ipr=1)

    if cmds.menuItem('AutoREL', q=1, cb=1):
        RELtoABS()

def SLiBHistoryCheck():
    global rootClone

    if cmds.objExists('SLiBBrowser_ImpHis') == 0:
        cmds.spaceLocator(n='SLiBBrowser_ImpHis')
        cmds.addAttr('SLiBBrowser_ImpHis', ln='ImportList_Objects', dt="string")
        cmds.setAttr('SLiBBrowser_ImpHis.ImportList_Objects', str(gib('file')) + '|', type='string')
        rootClone = None
        return 'Import'

    else:
        sceneObjects = cmds.ls(tr=1)

        if len(cmds.getAttr('SLiBBrowser_ImpHis.ImportList_Objects')) == 0:
            cmds.setAttr('SLiBBrowser_ImpHis.ImportList_Objects', str(gib('file')) + '|', type='string')
            rootClone = None
            return 'Import'

        else:
            cleanList = []
            cleanUpList = filter(None, cmds.getAttr('SLiBBrowser_ImpHis.ImportList_Objects').split('#'))
            for e in cleanUpList:
                if e.split('|')[1] in sceneObjects:
                    cleanList.append(str(e) + '#')

            rootClone = None
            for e in cleanList:
                if e.split('|')[0] == gib('file'):
                    rootClone = str(e.split('|')[1]).replace('#','')

            if rootClone == None:
                cmds.setAttr('SLiBBrowser_ImpHis.ImportList_Objects', str(''.join(cleanList)) + str(gib('file')) + '|', type='string')
                return 'Import'
            else:
                if cmds.menuItem('ReUseAsset', q=1, cb=1) == 1:
                    return 'Clone'
                else:
                    return 'Import'

def SLiBAddRoot(root):
    if rootClone == None:
        for a in filter(None, cmds.getAttr('SLiBBrowser_ImpHis.ImportList_Objects').split('#')):
            if a.split('|')[0] == gib('file'):
                cmds.setAttr('SLiBBrowser_ImpHis.ImportList_Objects', str(cmds.getAttr('SLiBBrowser_ImpHis.ImportList_Objects')) + str(root) + '#', type='string')

def SLiBShaderCheck(sel, root, importedShapes, importedMats, allMats):
    if gib('mainCat') != 'shader':
        i=1
        while cmds.objExists(root[0] + str(i).zfill(3)):
            i+=1
        root = cmds.rename(root[0], root[0] + '_' + str(i).zfill(3))

    shaderList = []
    mainShader = []

    for e in importedMats:
        if cmds.nodeType(e) != 'displacementShader':
            shaderList.append(e)

    if len(importedShapes) == 0:
        changed = 0
        if len(shaderList) > 1:
            print shaderList
            for e in shaderList:
                if cmds.nodeType(e) != 'displacementShader':
                    SG = cmds.listConnections(cmds.ls(e), type="shadingEngine")
                    if SG:
                        mainMat = cmds.ls(cmds.listConnections(SG), materials=1)
                        mainShader.append(mainMat)
                        shader = ','.join(mainShader[0])

                    else:
                        shader = shaderList[0]

        else:
            shader = shaderList[0]

        if cmds.menuItem('ReUseShader', q=1, cb=1):
            for a in allMats:
                if shader in a:
                    cmds.select(cmds.listHistory(shader), noExpand=1)
                    cmds.delete()
                    shader = a
                    print 'Shader with same Name already in scene. Reuse existing!',
                    changed = 1

        if len(sel) != 0:
            cmds.select(sel)
            for e in sel:
                if cmds.objectType(e) == 'transform' or cmds.objectType(e) == 'mesh':
                    cmds.hyperShade( a=shader )

        if changed != 1:
            cmds.rename(shader, shader + '_001')

def SLiBImportTexture():
    multiSel = SLiBMultiSel()

    for e in multiSel:
        cmds.iconTextRadioButton('icon' + e, e=1, sl=1)
        fileNode = cmds.shadingNode("file", asTexture=1)
        file = gib('file')
        cmds.setAttr( fileNode + '.fileTextureName', file, type = "string")
        SLiBMessager('IMPORT successful!', 'green')

def SLiBBrowserExport():
    shapesInSel =  cmds.ls(dag=1, o=1, s=1, sl=1)
    newShaderName = gib('name')
    assetPath = SLiBCurrLoc() + '/' + newShaderName

    if os.path.exists(assetPath):
        answer = cmds.confirmDialog( title='Warning', message='Shader/Asset with this Name already exists! \nDo you want to Update it?', button=['Update','No'], defaultButton='Update', cancelButton='No', dismissString='No' )
        if answer != 'Update':
            sys.exit()

    if gib('cat') == (None or 'Select...'):
        SLiBMessager('Please select a Category!', 'red')
        sys.exit()

    if len(gib('name')) == 0:
        SLiBMessager('Please enter a Name!', 'red')
        sys.exit()

    global selection
    try:
        selection = cmds.listRelatives(cmds.ls(sl=1)[0], f=1)[0].split('|')[1]
    except:
        SLiBMessager('Please select something!', 'red')
        sys.exit()

    if gib('mainCat') == 'shader':
        if len(shapesInSel) > 1:
            SLiBMessager('Please select one Object only!', 'red')
            sys.exit()

        if len(shapesInSel) != 1:
            SLiBMessager('Please select the Object that is holding your Shader!', 'red')
            sys.exit()

        else:
            shadingGrp = cmds.listConnections(shapesInSel, type='shadingEngine')
            if len(shadingGrp) > 1:
                try:
                    if cmds.window('exportWindow', ex=1):
                        cmds.deleteUI('exportWindow')
                except:
                    pass
                cmds.window('exportWindow', w=260, h=40, title="Multiple Shader found!", sizeable=1)
                cmds.columnLayout('shaderWinLayout', adj=1)
                cmds.text(l='More than one Shader found in Selection. \n Please select the one you want to Export!')
                cleanlist = []
                [cleanlist.append(x) for x in shadingGrp if x not in cleanlist]
                cmds.iconTextRadioCollection( 'itRadCollection' )
                for e in cleanlist:
                    if e == 'initialShadingGroup':
                        pass
                    else:
                        cmds.iconTextRadioButton( st='textOnly', h=50, l=e, p='shaderWinLayout')
                cmds.button('exportSelShader', l='Export', bgc=[0,0.75,0.99], c=lambda *args: (SLiBBrowserExportShader(shapesInSel, cmds.iconTextRadioButton(cmds.iconTextRadioCollection('itRadCollection', q=1, sl=1), q=1, l=1)), cmds.deleteUI('exportWindow')), p='shaderWinLayout')
                cmds.showWindow('exportWindow')
            else:
                SLiBBrowserExportShader(newShaderName, shapesInSel, shadingGrp)
    else:
        SLiBBrowserExportObject(newShaderName, shapesInSel)

def SLiBBrowserExportShader(newShaderName, shapesInSel, shadingGrp):
    assetPath = SLiBCurrLoc() + '/' + newShaderName

    file = assetPath + '/' + newShaderName
    cmds.select(shadingGrp, ne=1)

    cmds.progressBar('PreviewProgress', e=1, max=100)
    if os.path.isdir(assetPath) != True:
        os.mkdir(assetPath)
        cmds.progressBar('PreviewProgress', e=1, step=25)

    ### EXPORT WITH TEXTURE(S)
    if cmds.menuItem('exportTEX', q=1, cb=1) == 1:
        SLiBTexExport(shapesInSel)

    try:
        if cmds.menuItem('exportMA', q=1, rb=1) == 1:
            cmds.file(file + '.ma', op='v=0', typ = 'mayaAscii', es = 1)

        if cmds.menuItem('exportMB', q=1, rb=1) == 1:
            cmds.file(file + '.mb', op='v=0', typ = 'mayaBinary', es = 1)

        if cmds.menuItem('exportOBJ', q=1, rb=1) == 1:
            cmds.confirmDialog(m='No Shader Export as OBJ!')
    except:
        if cmds.menuItem('exportMA', q=1, rb=1) == 1:
            tempSavePath = cmds.workspace(q=1, fn=1) + "/" + newShaderName + '.ma'
            cmds.file(tempSavePath, op='v=0', typ = 'mayaAscii', es = 1)
            shutil.copyfile(tempSavePath, file + '.ma')
            cmds.sysFile(tempSavePath, delete=1)

        if cmds.menuItem('exportMB', q=1, rb=1) == 1:
            tempSavePath = cmds.workspace(q=1, fn=1) + "/" + newShaderName + '.mb'
            cmds.file(tempSavePath, op='v=0', typ = 'mayaBinary', es = 1)
            shutil.copyfile(tempSavePath, file + '.mb')
            cmds.sysFile(tempSavePath, delete=1)

        if cmds.menuItem('exportOBJ', q=1, rb=1) == 1:
            SLiBMessager('No Shader Export as OBJ!', 'red')

    cmds.progressBar('PreviewProgress', e=1, step=25)
    if cmds.iconTextButton('RenderViewButton', q=1, i=1) != file + '.png':
        shutil.copyfile(cmds.iconTextButton('RenderViewButton', q=1, i=1), file + '.png')
    notes = cmds.scrollField('SLiB_TEXTFIELD_Info', q=1, tx=1)
    notes = notes.replace(u"\u2018", "").replace(u"\u2019", "").replace(u"\u201c","").replace(u"\u201d", "")
    f = open(file + '.info', 'w')
    f.write(str(notes))
    f.close()

    cmds.progressBar('PreviewProgress', e=1, step=25)
    cmds.progressBar('PreviewProgress', e=1, pr=0)
    SLiBBrowserUpdateShader()
    SLiBMessager('EXPORT successful!', 'green')

    '''
    for l in os.listdir(assetPath):
        if os.path.isfile(os.path.join(assetPath,l)):
            if os.path.splitext(l)[1] in ['.mb', '.ma', '.obj']:
                cmds.progressBar('PreviewProgress', e=1, step=25)
                cmds.progressBar('PreviewProgress', e=1, pr=0)
                SLiBBrowserUpdateShader()
                SLiBMessager('EXPORT successful!', 'green')
            else:
                shutil.rmtree(assetPath)
                cmds.progressBar('PreviewProgress', e=1, pr=0)
                SLiBBrowserUpdateShader()'''

def SLiBBrowserExportObject(newShaderName, shapesInSel):
    assetPath = SLiBCurrLoc() + '/' + newShaderName
    file = assetPath + '/' + newShaderName

    cmds.progressBar('PreviewProgress', e=1, max=100)
    if os.path.isdir(assetPath) != True:
        os.mkdir(assetPath)
        cmds.progressBar('PreviewProgress', e=1, step=25)

    ### EXPORT WITH TEXTURE(S)
    if cmds.menuItem('exportTEX', q=1, cb=1) == 1:
        SLiBTexExport(shapesInSel)

    cmds.select(selection)

    ### DEL HISTORY
    if cmds.menuItem('exportHIS', q=1, cb=1) == True:
         cmds.delete(ch=1)

    ### AUTO PLACE PIVOT
    if cmds.menuItem('exportPIV', q=1, cb=1) == True:
        SLiBAutoPLacePivot()

    ### FREEZE
    if cmds.menuItem('exportFRZ', q=1, cb=1) == True:
        SLiBFreeze()

    try:
        if cmds.menuItem('exportMA', q=1, rb=1) == 1:
            cmds.file(file + '.ma', op='v=0', typ = 'mayaAscii', es = 1)

        if cmds.menuItem('exportMB', q=1, rb=1) == 1:
            cmds.file(file + '.mb', op='v=0', typ = 'mayaBinary', es = 1)

        if cmds.menuItem('exportOBJ', q=1, rb=1) == 1:
            cmds.file(file + '.obj', pr=1, typ="OBJexport", es=1, op="groups=0; ptgroups=0; materials=0; smoothing=0; normals=0")
    except:
        if cmds.menuItem('exportMA', q=1, rb=1) == 1:
            tempSavePath = cmds.workspace(q=1, fn=1) + "/" + newShaderName + '.ma'
            cmds.file(tempSavePath, op='v=0', typ = 'mayaAscii', es = 1)
            shutil.copyfile(tempSavePath, file + '.ma')
            cmds.sysFile(tempSavePath, delete=1)

        if cmds.menuItem('exportMB', q=1, rb=1) == 1:
            tempSavePath = cmds.workspace(q=1, fn=1) + "/" + newShaderName + '.mb'
            cmds.file(tempSavePath, op='v=0', typ = 'mayaBinary', es = 1)
            shutil.copyfile(tempSavePath, file + '.mb')
            cmds.sysFile(tempSavePath, delete=1)

        if cmds.menuItem('exportOBJ', q=1, rb=1) == 1:
            tempSavePath = cmds.workspace(q=1, fn=1) + "/" + newShaderName + '.obj'
            cmds.file(tempSavePath, pr = 1, typ="OBJexport", es = 1, op="groups=0; ptgroups=0; materials=0; smoothing=0; normals=0")
            shutil.copyfile(tempSavePath, file + '.obj')
            cmds.sysFile(tempSavePath, delete=1)

    cmds.progressBar('PreviewProgress', e=1, step=25)
    if cmds.iconTextButton('RenderViewButton', q=1, i=1) == file + '.png':
        pass
    else:
        shutil.copyfile(cmds.iconTextButton('RenderViewButton', q=1, i=1), file + '.png')
    notes = cmds.scrollField('SLiB_TEXTFIELD_Info', q=1, tx=1)
    notes = notes.replace(u"\u2018", "").replace(u"\u2019", "").replace(u"\u201c","").replace(u"\u201d", "")
    f = open(file + '.info', 'w')
    f.write(str(notes))
    f.close()
    dim = str(gib('size')).replace('(',"").replace(')',"")
    d = open(file + '.dim', 'w')
    d.write(dim)
    d.close()

    cmds.progressBar('PreviewProgress', e=1, step=25)
    cmds.progressBar('PreviewProgress', e=1, pr=0)
    SLiBBrowserUpdateShader()
    SLiBMessager('EXPORT successful!', 'green')
'''
for l in os.listdir(assetPath):
    if os.path.isfile(os.path.join(assetPath,l)):
        if os.path.splitext(l)[1] in ['.mb', '.ma', '.obj']:
            cmds.progressBar('PreviewProgress', e=1, step=25)
            cmds.progressBar('PreviewProgress', e=1, pr=0)
            SLiBBrowserUpdateShader()
            SLiBMessager('EXPORT successful!', 'green')
        else:
            shutil.rmtree(assetPath)
            cmds.progressBar('PreviewProgress', e=1, pr=0)
            SLiBBrowserUpdateShader()'''

def SLiBTexExport(shapesInSel):
    textdestination =  SLiBCurrLoc() + '/' + gib('name') + '/Tex'
    _shader = cmds.listHistory(shapesInSel, future=True)
    _history = cmds.listHistory(_shader)
    texlist = cmds.ls(_history, type="file")
    _missing = []

    if len(texlist) != 0:
        if os.path.isdir(textdestination) == False:
            os.mkdir(textdestination)

        for i in texlist:
            fileSource = cmds.getAttr("%s.fileTextureName" %i)
            if '${SLiBLib}' in fileSource:
                fileSource = fileSource.replace('${SLiBLib}', mel.eval('getenv SLiBLib;'))

            if os.path.isfile(fileSource) == True:
                justName = fileSource.split("/")[-1]
                fileDest = textdestination + '/' + justName
                if fileSource != fileDest:
                    if os.path.isfile(fileDest) == False:
                        shutil.copy(fileSource, fileDest)
                        relativePath = fileDest.split('lib')
                        relativePath = '${SLiBLib}' + relativePath[1]
                        if cmds.menuItem('exportREL', q=1, rb=1) == True:
                            cmds.setAttr("%s.fileTextureName" %i, relativePath, type="string")
                        if cmds.menuItem('exportABS', q=1, rb=1) == True:
                            cmds.setAttr("%s.fileTextureName" %i, fileDest, type="string")
                else:
                    pass
            else:
                _missing.append(fileSource)
                pass

    if getRenderEngine() == 'redshift':
        texlistN = cmds.ls(_history, type='RedshiftNormalMap')
        if len(texlistN) != 0:
            if os.path.isdir(textdestination) == False:
                os.mkdir(textdestination)

            for i in texlistN:
                fileSource = cmds.getAttr("%s.tex0" %i)
                if '${SLiBLib}' in fileSource:
                    fileSource = fileSource.replace('${SLiBLib}', mel.eval('getenv SLiBLib;'))
                if os.path.isfile(fileSource) == True:
                    justName = fileSource.split("/")[-1]
                    fileDest = textdestination + '/' + justName
                    if fileSource != fileDest:
                        if os.path.isfile(fileDest) == False:
                            shutil.copy(fileSource, fileDest)
                            relativePath = fileDest.split('lib')
                            relativePath = '${SLiBLib}' + relativePath[1]
                            if cmds.menuItem('exportREL', q=1, rb=1) == True:
                                cmds.setAttr("%s.tex0" %i, relativePath, type="string")
                            if cmds.menuItem('exportABS', q=1, rb=1) == True:
                                cmds.setAttr("%s.tex0" %i, fileDest, type="string")
                    else:
                        pass
                else:
                    _missing.append(fileSource)
                    pass

    if len(_missing) != 0:
        cmds.confirmDialog(m='Texture File(s) missing! Check Script Editor for details.')
        print ''
        print 'MISSING TEXTURE FILE(S) >>> START'
        for m in _missing:
            print m + ' < is missing!'
        print 'MISSING TEXTURE FILE(S) <<< END'

def SLiBFlushOptionMenu(flushOptionMenu):
    if cmds.iconTextCheckBox('searchSwitch', q=1, v=1) == 1:
        cmds.textField('SLiB_TEXTFIELD_Search', e=1, tx='')
        cmds.iconTextCheckBox('searchSwitch', e=1, v=0)
    try:
        menuItems = cmds.optionMenu(flushOptionMenu, q=1, itemListLong=1)
        for curItem in menuItems:
            cmds.deleteUI(curItem, menuItem=1)
    except:
        pass
    SLiBswitchExportButton()
    SLiBTypeBGChange()

def SLiBTypeBGChange():
    allTypes = cmds.iconTextRadioCollection( 'mainCatCollection', q=1, cia=1)
    for e in allTypes:
        cmds.iconTextRadioButton(e.split('|')[-1], e=1, bgc=[0,0,0])
    cmds.iconTextRadioButton(cmds.iconTextRadioCollection( 'mainCatCollection', q=1, sl=1 ), e=1, bgc=[0,0.75,0.99])

#UPDATE INFO
def SLiBBrowserUpdateInfo():
    if cmds.window('zoomWin', ex=1):
        cmds.deleteUI('zoomWin')
    try:
        cmds.iconTextRadioButton('icon'+lastasset, e=1, bgc=[0.15,0.15,0.15] )
    except:
        pass
    assetPath = gib('fullPath').rsplit('/', 1)[0]
    asset = cmds.iconTextRadioButton(cmds.iconTextRadioCollection('slAssetCollection', q=1, sl=1), q=1, annotation=1)
    if gib('mainCat') == 'textures':
        assetPath = SLiBCurrLoc()
        imageFile = cmds.iconTextRadioButton(cmds.iconTextRadioCollection('slAssetCollection', q=1, sl=1), q=1, i=1)
    else:
        try:
            f = open(assetPath + '/' + asset + '.info', 'r')
            importNotes = f.read()
            printNotes = str(importNotes)
            cmds.scrollField('SLiB_TEXTFIELD_Info', e=1, tx=printNotes)
        except:
            pass
        imageFile = assetPath + '/' + asset + '.png'
    if cmds.iconTextCheckBox('RVLocked', q=1, v=1) == 0:
        cmds.iconTextButton('RenderViewButton', e=1, i=imageFile)
    cmds.text('SLiB_shaderName', e=1, l=asset, al='center')

    file = asset
    if cmds.iconTextCheckBox('SLiBIconMark', q=1, v=1):
        if cmds.checkBox('cb_'+file, q=1, v=1) == 0:
            cmds.checkBox('cb_' + file, e=1, v=1)
            SLiBcbToggleOn(file)
        else:
            cmds.checkBox('cb_' + file, e=1, v=0)
            SLiBcbToggleOff(file)
    else:
        for e in cmds.iconTextRadioCollection('slAssetCollection', q=1, cia=1):
            all = e.split('|icon')[-1]
            cmds.checkBox('cb_' + all, e=1, v=0)
            SLiBcbToggleOff(all)
        cmds.checkBox('cb_' + file, e=1, v=1)
        SLiBcbToggleOn(file)

    SLiBMessager('Selection Info updated!', 'none')

    cmds.iconTextRadioButton('icon'+asset, e=1, bgc=[0,0.75,0.99] )
    global lastasset
    lastasset = asset

def zoomWinOn():
    if cmds.window('zoomWin', ex=1):
        cmds.deleteUI('zoomWin')
    if cmds.iconTextButton('RenderViewButton', q=1, i=1) !=SLiBImage + 'browser_logo.png':
        cmds.window('zoomWin', title="", sizeable=0)
        cmds.rowColumnLayout('zoomLayout', nc=1, w=512, h=512, bgc=[0.1,0.1,0.1], p='zoomWin')
        cmds.rowColumnLayout('zoomButtonLayout', nc=3, w=512, h=18, p='zoomLayout')
        cmds.iconTextButton(l='0', h=16, w=171, st='textOnly', c=lambda *arghs: cmds.rowColumnLayout('zoomLayout', e=1, bgc=[0,0,0]), bgc=[0,0,0], p='zoomButtonLayout')
        cmds.iconTextButton(l='0.5', h=16, w=170, st='textOnly', c=lambda *arghs: cmds.rowColumnLayout('zoomLayout', e=1, bgc=[0.5,0.5,0.5]), bgc=[0.5,0.5,0.5], p='zoomButtonLayout')
        cmds.iconTextButton(l='1', h=16, w=171, st='textOnly', c=lambda *arghs: cmds.rowColumnLayout('zoomLayout', e=1, bgc=[1,1,1]), bgc=[1,1,1], p='zoomButtonLayout')
        cmds.iconTextButton(image=cmds.iconTextButton('RenderViewButton', q=1, i=1), w=512, h=512, c=lambda *arghs: zoomWinOff(), p='zoomLayout')
        cmds.window('zoomWin', e=1, w=512, h=530)
        zoomWinWrapped = shiboken.wrapInstance(long(omui.MQtUtil.findControl('zoomWin')), QtGui.QWidget)
        zoomWinWrapped.setWindowFlags(QtCore.Qt.Tool|QtCore.Qt.WA_MouseTracking)
        zoomWinWrapped.setWindowFlags(QtCore.Qt.Tool|QtCore.Qt.WA_TranslucentBackground)
        zoomWinWrapped.setMouseTracking(True)
        zoomWinWrapped.setWindowFlags(QtCore.Qt.Tool|QtCore.Qt.FramelessWindowHint)
        zoomWinWrapped.show()
        screen = QtGui.QDesktopWidget().screenGeometry()
        zoomWinWrapped.move(screen.width()/2-256, screen.height()/2-256)

def zoomWinOff():
    if cmds.window('zoomWin', ex=1):
        cmds.deleteUI('zoomWin')

#UPDATE SHADER
def SLiBBrowserUpdateShader():
    if gib('mainCat') == 'textures':
        SLiBBrowserUpdateTextures()
    else:
        if cmds.scrollLayout('SLiBScrollLayoutBrowser', q=1, ex=1):
            cmds.deleteUI('SLiBScrollLayoutBrowser', layout=1)

        cmds.scrollLayout('SLiBScrollLayoutBrowser', bgc=[0.15,0.15,0.15], p="SLiB_thumbsframe")
        cmds.popupMenu(parent='SLiBScrollLayoutBrowser', ctl=0, button=3)
        cmds.menuItem(l='Paste', c=lambda *args: SLiBBrowserPaste())
        cmds.menuItem(d=1)
        cmds.menuItem(l='Select All', c=lambda *args: SLiBBrowserMark('all'))
        cmds.menuItem(l='Select None', c=lambda *args: SLiBBrowserMark('none'))

        iconSize = cmds.textField('SLiBThumbSizeComboBox', q=1, tx=1)
        if int(iconSize) > 1024:
            iconSize = [1024]
            cmds.textField('SLiBThumbSizeComboBox', e=1, tx=iconSize[0])

        coll = cmds.textField('SLiBThumbColumnsComboBox', q=1, tx=1)
        cmds.rowColumnLayout('Icons', nc=int(coll), p="SLiBScrollLayoutBrowser")

        allDirs = []
        if cmds.iconTextCheckBox('searchSwitch', q=1, v=1) == 1:
            if cmds.menuItem('searchCat', q=1 , rb=1) == 1:
                searchLoc = mel.eval('getenv SLiBLib;') + gib('mainCat') + '/' + gib('cat')
                if 'Select..' in searchLoc:
                    SLiBMessager('Please select a Category!', 'red')
                    sys.exit()
            if cmds.menuItem('searchLib', q=1, rb=1) == 1:
                searchLoc = mel.eval('getenv SLiBLib;') + gib('mainCat')

            for root, dirs, files in os.walk(searchLoc):
                for name in dirs:
                    if name != '.mayaSwatches' and name != '_SUB' and name != 'Tex' and name != '.DS_Store':
                        allDirs.append(os.path.join(root, name))

            fWord = cmds.textField('SLiB_TEXTFIELD_Search', q=1, tx=1)
            if len(fWord) == 0:
                sys.exit()
            else:
                matchDirs = [k for k in allDirs if fWord.lower() in k.lower()]
                allDirs = matchDirs

        else:
            if gib('cat') == 'Select...':
                sys.exit()
            dir = SLiBCurrLoc()
            for name in os.listdir(dir):
                if name != '.mayaSwatches' and name != '_SUB' and name != 'Tex' and name != '.DS_Store':
                    allDirs.append(os.path.join(dir, name))

        SLiBCollection = []
        for dir in allDirs:
            for a in os.listdir(dir):
                if os.path.splitext(a)[1] == '.mb' or os.path.splitext(a)[1] == '.ma' or os.path.splitext(a)[1] == '.obj':
                    SLiBCollection.append(dir + '/' + a)

        if len(SLiBCollection) != 0:
            cmds.iconTextRadioCollection('slAssetCollection')
            cmds.progressBar('PreviewProgress', e=1, pr=0)
            cmds.progressBar('PreviewProgress', e=1, max=(len(SLiBCollection)))

        if cmds.iconTextCheckBox('searchSwitch', q=1, v=1) == 1:
            cleanList = []
            matchList = []
            for e in SLiBCollection:
                fileDir = os.path.dirname(e)
                fileBase = os.path.basename(e)
                if str(fileBase) in matchList:
                    print fileBase
                else:
                    cleanList.append(e)
                    matchList.append(fileBase)

            SLiBCollection = cleanList

        for s in SLiBCollection:
            s = str(s).replace("\\", "/")
            fileDir = os.path.dirname(s)
            fileBase = os.path.basename(s)
            file = os.path.splitext(fileBase)[0]
            fileEx = os.path.splitext(fileBase)[1]
            image = fileDir + '/' + file + '.png'
            cmds.columnLayout('cell'+file, rowSpacing=2, adj=1, cal='center', cw = int(iconSize)+6, p='Icons')
            cmds.iconTextRadioButton('icon'+file , i=image, mw=3, mh=3, w=int(iconSize)+7, h=int(iconSize)+7, onc=lambda *args: SLiBBrowserUpdateInfo(), l=s, ann=file, p='cell'+file)
            cmds.popupMenu('pop'+file, parent='icon'+file, ctl=0, button=3, pmc=partial(SLiBBrowserPostMenu, file))
            cmds.menuItem(l=file, i=image, bld=1, en=0)
            cmds.menuItem(l='', en=0)
            cmds.menuItem(l='IMPORT', c=lambda *args: SLiBBrowserImport('Normal'))
            cmds.menuItem(d=1)
            if ('/objects/' or '/lights/') in image:
                cmds.menuItem(l='IMPORT and Place at Selection', c=lambda *args: SLiBBrowserImport('Place'))
                cmds.menuItem(l='IMPORT and Replace Selection', c=lambda *args: SLiBBrowserImport('Replace'))
                cmds.menuItem(d=1)
            cmds.menuItem(l='OPEN in Maya', c=lambda *args: SLiBOpenInMaya())
            cmds.menuItem(l='OPEN in FileBrowser', c=lambda *args: SLiBOpenInFileBrowser())
            cmds.menuItem(d=1)
            cmds.menuItem(l='RENAME', c=lambda *args: SLiBBrowserRename())
            cmds.menuItem(d=1)
            cmds.menuItem(l='COPY', c=lambda *args: SLiBBrowserCopy())
            cmds.menuItem(d=1)
            cmds.menuItem(l='DUPLICATE', c=lambda *args: SLiBBrowserDuplicate())
            cmds.menuItem(d=1)
            cmds.menuItem(l='DELETE', c=lambda *args: SLiBBrowserDelete())
            cmds.rowLayout('caption'+file, w=int(iconSize)-6, nc=3, adj=2, cal=(1, 'center'), bgc=[0.1,0.1,0.1], p='cell'+file)
            cmds.checkBox('cb_'+file, l='', onc=partial(SLiBcbToggleOn, file), ofc=partial(SLiBcbToggleOff, file))
            #cmds.text('lbl_' + file, label = ' ' + file, align = 'left', bgc=[0.1,0.1,0.1], p='caption'+file)
            cmds.text('lbl_' + file, l=' ' + file, al='left', p='caption'+file)
            if fileEx == '.ma':
                cmds.iconTextButton(i=SLiBImage + 'SLiB_ma.png', mw=0, mh=0, h=16, w=16, p='caption'+file)
            if fileEx == '.mb':
                cmds.iconTextButton(i=SLiBImage + 'SLiB_mb.png', mw=0, mh=0, h=16, w=16, p='caption'+file)
            if fileEx == '.obj':
                cmds.iconTextButton(i=SLiBImage + 'SLiB_obj.png', mw=0, mh=0, h=16, w=16, p='caption'+file)
            if cmds.iconTextCheckBox('SLiBIconCaption', q=1, v=1):
                cmds.rowLayout('caption'+file, e=1, vis=1)
            else:
                cmds.rowLayout('caption'+file, e=1, vis=0)
            cmds.progressBar('PreviewProgress', e=1, step=1)

        cmds.progressBar('PreviewProgress', e=1, pr=0)
        SLiBSaveCats()

        if len(SLiBCollection) != 0:
            if cmds.iconTextCheckBox('searchSwitch', q=1, v=1) == 1:
                if gib('mainCat') == 'shader':
                    SLiBMessager(str(len(SLiBCollection)) + ' Material(s) found!', 'green')
                if gib('mainCat') == 'objects':
                    SLiBMessager(str(len(SLiBCollection)) + ' Asset(s) found!', 'green')
                if gib('mainCat') == 'lights':
                    SLiBMessager(str(len(SLiBCollection)) + ' Light(s) found!', 'green')
                if gib('mainCat') == 'textures':
                    SLiBMessager(str(len(SLiBCollection)) + ' Texture(s) found!', 'green')
            else:
                if gib('mainCat') == 'shader':
                    SLiBMessager(str(len(SLiBCollection)) + ' Material(s) loaded!', 'green')
                if gib('mainCat') == 'objects':
                    SLiBMessager(str(len(SLiBCollection)) + ' Asset(s) loaded!', 'green')
                if gib('mainCat') == 'lights':
                    SLiBMessager(str(len(SLiBCollection)) + ' Light(s) loaded!', 'green')
                if gib('mainCat') == 'textures':
                    SLiBMessager(str(len(SLiBCollection)) + ' Texture(s) loaded!', 'green')
        else:
            if cmds.iconTextCheckBox('searchSwitch', q=1, v=1) == 1:
                SLiBMessager('Nothing found!', 'yellow')
            else:
                SLiBMessager('Category is empty!', 'yellow')

def SLiBBrowserPostMenu(file, *args):
    cmds.iconTextRadioButton('icon'+file, e=1, sl=1)
    SLiBBrowserUpdateInfo()

def SLiBBrowserUpdateTextures():
    if cmds.scrollLayout('SLiBScrollLayoutBrowser', q=1, ex=1):
        cmds.deleteUI('SLiBScrollLayoutBrowser', layout=1)

    cmds.scrollLayout('SLiBScrollLayoutBrowser', bgc=[0.15,0.15,0.15], p="SLiB_thumbsframe")
    cmds.popupMenu(parent='SLiBScrollLayoutBrowser', ctl=0, button=3)
    cmds.menuItem(l='Paste', c=lambda *args: SLiBBrowserPaste())

    iconSize = cmds.textField('SLiBThumbSizeComboBox', q=1, tx=1)
    if int(iconSize) > 1024:
        iconSize = [1024]
        cmds.textField('SLiBThumbSizeComboBox', e=1, tx=iconSize[0])

    coll = cmds.textField('SLiBThumbColumnsComboBox', q=1, tx=1)
    cmds.rowColumnLayout('Icons', nc=int(coll), p="SLiBScrollLayoutBrowser")

    allTex = []
    if cmds.iconTextCheckBox('searchSwitch', q=1, v=1) == 1:
        if cmds.menuItem('searchCat', q=1 , rb=1) == 1:
            searchLoc = mel.eval('getenv SLiBLib;') + gib('mainCat') + '/' + gib('cat')
            if 'Select..' in searchLoc:
                SLiBMessager('Please select a Category!', 'red')
                sys.exit()
        if cmds.menuItem('searchLib', q=1, rb=1) == 1:
            searchLoc = mel.eval('getenv SLiBLib;') + gib('mainCat')

        for root, dirs, files in os.walk(searchLoc):
            for name in files:
                nameExt = os.path.splitext(name)[1]
                if nameExt != '.swatch':
                    print files
                    allTex.append(os.path.join(root, name))

        fWord = cmds.textField('SLiB_TEXTFIELD_Search', q=1, tx=1)
        if len(fWord) == 0:
            sys.exit()
        else:
            matchDirs = [k for k in allTex if fWord.lower() in k.lower()]
            allTex = matchDirs

    else:
        if gib('cat') == 'Select...':
            sys.exit()

        dir = SLiBCurrLoc()
        for name in os.listdir(dir):
            if name != '.mayaSwatches' and name != '_SUB' and name != 'Tex' and name != '.DS_Store' and name != '_THUMBS':
                allTex.append(os.path.join(dir, name))

    if len(allTex) != 0:
        cmds.iconTextRadioCollection('slAssetCollection')
        cmds.progressBar('PreviewProgress', e=1, pr=0)
        cmds.progressBar('PreviewProgress', e=1, max=(len(allTex)))

    if cmds.iconTextCheckBox('searchSwitch', q=1, v=1) == 1:
        cleanList = []
        matchList = []
        for e in allTex:
            fileDir = os.path.dirname(e)
            fileBase = os.path.basename(e)
            if str(fileBase) in matchList:
                print fileBase
            else:
                cleanList.append(e)
                matchList.append(fileBase)

        allTex = cleanList

    for tex in allTex:
        tex = str(tex).replace("\\", "/")
        fileDir = os.path.dirname(tex)
        fileBase = os.path.basename(tex)
        file = os.path.splitext(fileBase)[0]
        fileEx = os.path.splitext(fileBase)[1]
        cmds.progressBar('PreviewProgress', e=1, step=1)
        if os.path.isfile(fileDir + '/_THUMBS' + '/' + fileBase) == True:
            image = fileDir + '/_THUMBS' + '/' + fileBase
        elif os.path.isfile(fileDir + '/_THUMBS' + '/' + file + '.png') == True:
            image = fileDir + '/_THUMBS' + '/' + file + '.png'
        elif os.path.isfile(fileDir + '/_THUMBS' + '/' + file + '.jpg') == True:
            image = fileDir + '/_THUMBS' + '/' + file + '.jpg'
        else:
            SLiBMessager('Texture Preview Image(s) not found! Using Original...', 'yellow')
            image = fileDir + '/' + fileBase
        cmds.columnLayout('cell'+file, rowSpacing=2, adj=1, cal='center', cw = int(iconSize)+6, p='Icons')
        assetIcon = cmds.iconTextRadioButton('icon'+file, i=image, mw=3, mh=3, h=int(iconSize), w=int(iconSize), onc=lambda *args: SLiBBrowserUpdateInfo(), l=tex, ann=file)
        cmds.popupMenu(parent='icon'+file, ctl=0, button=3, pmc=partial(SLiBBrowserPostMenu, file))
        cmds.menuItem(l=file, i=image, bld=1, en=0)
        cmds.menuItem(l='', en=0)
        cmds.menuItem(l='IMPORT', c=lambda *args: SLiBImportTexture())
        cmds.menuItem( d=1 )
        cmds.menuItem(l='OPEN in FileBrowser', c=lambda *args: SLiBOpenInFileBrowser())
        cmds.menuItem( d=1 )
        cmds.menuItem(l='Rename', c=lambda *args: SLiBBrowserRename())
        cmds.menuItem( d=1 )
        cmds.menuItem(l='Copy', c=lambda *args: SLiBBrowserCopy())
        cmds.menuItem( d=1 )
        cmds.menuItem(l='Duplicate', c=lambda *args: SLiBBrowserDuplicate())
        cmds.menuItem( d=1 )
        cmds.menuItem(l='Delete', c=lambda *args: SLiBBrowserDelete())
        cmds.rowLayout('caption'+file, w=int(iconSize)-6, nc=2, adj=2, cal=(1, 'center'), bgc=[0.1,0.1,0.1], p='cell'+file)
        cmds.checkBox('cb_'+file, l='', onc=partial(SLiBcbToggleOn, file), ofc=partial(SLiBcbToggleOff, file))
        cmds.text('lbl_' + file, l=' ' + file, al='left')
        if cmds.iconTextCheckBox('SLiBIconCaption', q=1, v=1):
            cmds.rowLayout('caption'+file, e=1, vis=1)
        else:
            cmds.rowLayout('caption'+file, e=1, vis=0)

    cmds.progressBar('PreviewProgress', e=1, pr=0)
    SLiBSaveCats()

    if len(allTex) != 0:
        if cmds.iconTextCheckBox('searchSwitch', q=1, v=1) == 1:
            SLiBMessager(str(len(allTex)) + ' Texture(s) found!', 'green')
        else:
            SLiBMessager(str(len(allTex)) + ' Texture(s) loaded!', 'green')
    else:
        if cmds.iconTextCheckBox('searchSwitch', q=1, v=1) == 1:
            SLiBMessager('Nothing found!', 'yellow')
        else:
            SLiBMessager('Category is empty!', 'yellow')

def SLiBBrowserUpdateType():
    SLiBFlushOptionMenu('SLiB_cb_Cat')
    print 'SLiB >>> Category Menu flushed!'
    typeList =  os.listdir(gib('library') + gib('mainCat') + '/')
    if len(typeList) != 0:
        cmds.optionMenu('SLiB_cb_Cat', e=1, enable=1, cc=lambda *args: SLiBBrowserUpdateSubType())
        cmds.setParent('SLiB_cb_Cat', menu=1)
        cmds.menuItem(l='Select...')
        for curType in typeList:
            if curType != '.DS_Store':
                cmds.menuItem(l=curType)
        #cmds.optionMenu('SLiB_cb_Cat', e=1, v='Select...')
    else:
        if cmds.scrollLayout('SLiBScrollLayoutBrowser', q=1, ex=1):
            cmds.deleteUI('SLiBScrollLayoutBrowser', layout = 1)
    SLiBBrowserUpdateSubType()

def SLiBBrowserUpdateTypeOnly():
    SLiBFlushOptionMenu('SLiB_cb_Cat')
    print 'SLiB >>> Category Menu flushed!'
    typeList =  os.listdir(gib('library') + gib('mainCat') + '/')
    if len(typeList) != 0:
        typeList = list(typeList)
        cmds.optionMenu('SLiB_cb_Cat', e=1, enable=1)
        cmds.setParent('SLiB_cb_Cat', menu=1)
        cmds.menuItem(l='Select...')
        for curType in typeList:
            if curType != '.DS_Store':
                cmds.menuItem(l=curType)

#UPDATE SUB CAT
def SLiBBrowserUpdateSubType():
    SLiBFlushOptionMenu('SLiB_cb_SubCat')
    print 'SLiB >>> SubCategory Menu flushed!'
    if gib('cat') == 'Select...':
        if cmds.scrollLayout('SLiBScrollLayoutBrowser', q=1, ex=1):
            cmds.deleteUI('SLiBScrollLayoutBrowser', layout=1)
            sys.exit()
    else:
        try:
            subTypeList = os.listdir(gib('library') + gib('mainCat') + '/' + gib('cat') + '/_SUB')
        except:
            os.mkdir(gib('library') + gib('mainCat') + '/' + gib('cat') + '/_SUB')
            subTypeList = os.listdir(gib('library') + gib('mainCat') + '/' + gib('cat') + '/_SUB')

        if len(subTypeList) != 0:
            cmds.optionMenu('SLiB_cb_SubCat', e=1, enable=1, cc=lambda *args: SLiBBrowserUpdateShader())
            cmds.setParent('SLiB_cb_SubCat', menu = 1)
            cmds.menuItem(l='Select...')
            for curType in subTypeList:
                if curType != '.DS_Store':
                    cmds.menuItem(l=curType)
            #cmds.optionMenu('SLiB_cb_SubCat', e=1, v='Select...')
            SLiBBrowserUpdateShader()
        else:
            cmds.optionMenu('SLiB_cb_SubCat', e=1, enable=0)
            SLiBBrowserUpdateShader()

def SLiBBrowserUpdateSubTypeOnly():
    SLiBFlushOptionMenu('SLiB_cb_SubCat')
    print 'SLiB >>> SubCategory Menu flushed!'
    if gib('cat') == 'Select...':
        if cmds.scrollLayout('SLiBScrollLayoutBrowser', q=1, ex=1):
            cmds.deleteUI('SLiBScrollLayoutBrowser', layout=1)
            sys.exit()
    else:
        subTypeList = os.listdir(gib('library') + gib('mainCat') + '/' + gib('cat') + '/_SUB')
        if len(subTypeList) != 0:
            cmds.optionMenu('SLiB_cb_SubCat', e=1, enable=1, cc=lambda *args: SLiBBrowserUpdateShader())
            cmds.setParent('SLiB_cb_SubCat', menu=1)
            cmds.menuItem(l='Select...')
            for curType in subTypeList:
                if curType != '.DS_Store':
                    cmds.menuItem(l=curType)
        else:
            cmds.optionMenu('SLiB_cb_SubCat', e=1, enable=0)

def SLiBOpenInMaya():
    answer = cmds.confirmDialog( title='Warning', message='This will close the current scene! \nDo you want to proceed?', button=['Yes','No'], defaultButton='Yes', cancelButton='No', dismissString='No' )
    if answer == 'Yes':
        cmds.file(gib('file'), open=1, force=1)
    else:
        sys.exit()

def SLiBOpenInFileBrowser():
    if gib('mainCat') == 'textures':
        path = SLiBCurrLoc()
    else:
        path = str(os.path.dirname(gib('file')))
        path = '\\'.join(path.split('/'))

    if platform.system() == "Windows":
        os.startfile(path)
    elif platform.system() == "Darwin":
        subprocess.Popen(["open", path])
    else:
        subprocess.Popen(["xdg-open", path])

def SLiBOpenCatInFileBrowser():
    if gib('cat') != 'Select...' and gib('cat') != None:
        path = gib('library') + gib('mainCat') + '/' + gib('cat')

        if platform.system() == "Windows":
            os.startfile(path)
        elif platform.system() == "Darwin":
            subprocess.Popen(["open", path])
        else:
            subprocess.Popen(["xdg-open", path])
    else:
        SLiBMessager('Please select a Category first!', 'red')

def SLiBOpenSubCatInFileBrowser():
    if gib('subCat') != 'Select...' and gib('subCat') != None:
        path = gib('library') + gib('mainCat') + '/' + gib('cat') + '/_SUB/' + gib('subCat')

        if platform.system() == "Windows":
            os.startfile(path)
        elif platform.system() == "Darwin":
            subprocess.Popen(["open", path])
        else:
            subprocess.Popen(["xdg-open", path])
    else:
        SLiBMessager('Please select a Sub-Category first!', 'red')

def SLiBNameFromSelection():
    sel = cmds.ls(sl=1)
    if len(sel) == 0:
        SLiBMessager('Please select an Oject to get the Name from!', 'red')
        sys.exit()
    cmds.textField('SLiB_TEXTFIELD_Name', e=1, tx=sel[0])

def SLiBBrowserDelete():
    if cmds.menuItem('QuickDelete', q=1, cb=1) == 1:
        answer = cmds.confirmDialog( title='Warning', message='Do you really want to Delete? \nPress YES to proceed.', button=['Yes','No'], defaultButton='Yes', cancelButton='No', dismissString='No' )
    else:
        answer = 'Yes'

    if answer == 'Yes':
        multiSel = SLiBMultiSel()
        if gib('mainCat') == 'textures':
            for e in multiSel:
                cmds.iconTextRadioButton('icon' + e, e=1, sl=1)
                deleteFolder = gib('file')
                cmds.sysFile( deleteFolder, delete=1 )
        else:
            for e in multiSel:
                cmds.iconTextRadioButton('icon' + e, e=1, sl=1)
                deleteFolder = os.path.dirname(gib('file'))
                shutil.rmtree(deleteFolder)

        if cmds.iconTextCheckBox('RVLocked', q=1, v=1) == 0:
            cmds.iconTextButton('RenderViewButton', e=1, i=SLiBImage + 'browser_logo.png')
            cmds.popupMenu(parent="RenderViewButton", ctl=0, button=3)
            cmds.menuItem(l='Replace Preview Image', c=lambda *args: SLiBReplacePreview())
            cmds.menuItem(d=1)
            cmds.menuItem(l='Reset', c=lambda *args: cmds.iconTextButton('RenderViewButton', e=1, i=SLiBImage + 'browser_logo.png'))
        cmds.text('SLiB_shaderName', e=1, l='', al='center')
        cmds.evalDeferred(lambda: SLiBBrowserUpdateShader() )
        SLiBMessager('Removed!', 'green')

def SLiBBrowserRename():
    result = cmds.promptDialog(title='Rename', message='Enter Name:', button=['OK', 'Cancel'], defaultButton='OK', cancelButton='Cancel', dismissString='Cancel')
    if result == 'OK':
        if gib('mainCat') == 'textures':
            renameFolder = gib('file')
            renameExt = os.path.splitext(gib('file'))[1]
            newName = cmds.promptDialog(q=1, t=1).replace(' ','_')
            cmds.sysFile(renameFolder, rename=SLiBCurrLoc() + '/' + newName + renameExt )
            cmds.evalDeferred(lambda: SLiBBrowserUpdateShader())
            SLiBMessager('Renamed!', 'green')

        else:
            renameFolder = os.path.dirname(gib('file'))
            renameFile = os.path.splitext(gib('file'))[0]
            newName = cmds.promptDialog(q=1, t=1).replace(' ','_')
            newDir = gib('file').rsplit('/', 2)[0] + '/' + newName
            if os.path.isdir(newDir) == 1:
                SLiBMessager('Name already exists!', 'red')
                sys.exit()
            else:
                shutil.move(renameFolder, newDir)

            allFiles = [os.path.join(newDir,fn) for fn in next(os.walk(newDir))[2]]

            for e in allFiles:
                file = os.path.splitext(e)[0]
                fileExt = os.path.splitext(e)[1]
                os.rename(e, newDir + "/" + newName + fileExt)
            path = newDir

            SLiBMessager('Renamed!', 'green')
            cmds.evalDeferred(lambda: (SLiBBrowserUpdateShader(), cmds.iconTextRadioButton('icon'+newName, e=1, sl=1)) )
            SLiBTexPathChangeWarning(path)
    else:
        sys.exit()

def SLiBBrowserDuplicate():
    multiSel = SLiBMultiSel()
    for e in multiSel:
        cmds.iconTextRadioButton('icon' + e, e=1, sl=1)
        if gib('mainCat') == 'textures':
            doppelFolder = os.path.splitext(gib('file'))[0]
            doppelExt = os.path.splitext(gib('file'))[1]
            i=1
            while os.path.exists(doppelFolder + "_Copy_" + str(i) + doppelExt):
                i+=1
            shutil.copy(doppelFolder + doppelExt, doppelFolder + "_Copy_" + str(i) + doppelExt)
        else:
            doppelFolder = os.path.dirname(gib('file'))
            doppelName = doppelFolder.split('/')[-1]
            destFolder = SLiBCurrLoc() + '/' + doppelName + '_Copy'
            i=1
            while os.path.exists(destFolder + str(i)):
                i+=1
            ziel = destFolder + str(i)
            shutil.copytree(doppelFolder, ziel)
            allFiles = [os.path.join(ziel,fn) for fn in next(os.walk(ziel))[2]]
            for e in allFiles:
                file = os.path.splitext(e)[0]
                fileExt = os.path.splitext(e)[1]
                os.rename(e, file + "_Copy" + str(i)  + fileExt)
            path = destFolder + str(i)
            SLiBTexPathChangeWarning(path)

    SLiBMessager('Duplicated!', 'green')
    cmds.evalDeferred(lambda: SLiBBrowserUpdateShader())

def SLiBBrowserCopy():
    global copyExt
    global copyExts
    global copyFolder
    global copyFolders
    global copyFile
    global copyFiles

    multiSel = SLiBMultiSel()
    copyFolders = []
    copyFiles = []
    copyExts = []
    if gib('mainCat') == 'textures':
        for e in multiSel:
            cmds.iconTextRadioButton('icon' + e, e=1, sl=1)
            copyFolder = gib('file')
            copyExt = os.path.splitext(gib('file'))[1]
            copyFolders.append(copyFolder)
            copyExts.append(copyExt)
    else:
        for e in multiSel:
            cmds.iconTextRadioButton('icon' + e, e=1, sl=1)
            copyFolder = os.path.dirname(gib('file'))
            copyFile = os.path.splitext(gib('file').rsplit('/', 1)[1])[0]
            copyExt = os.path.splitext(gib('file').rsplit('/', 1)[1])[1]
            copyFolders.append(copyFolder)
            copyFiles.append(copyFile)
            copyExts.append(copyExt)

    SLiBMessager('Copied!', 'green')

def SLiBBrowserPaste():
    if len(copyFolders) != 0:
        if gib('mainCat') == 'textures':
            for r in range(0, len(copyFolders)-1):
                if copyFolders[r] == SLiBCurrLoc() + '/' + copyFiles[r] + copyExts[r]:
                    SLiBMessager('Cannot COPY and PASTE to the same Location! Use DUPLICATE!', 'red')
                    sys.exit()
                else:
                    shutil.copy(copyFolders[r], SLiBCurrLoc() + '/' + copyFiles[r] + copyExts[r])
                    cmds.evalDeferred(lambda: SLiBBrowserUpdateShader())
                    SLiBMessager('Pasted!', 'green')
        else:
            if cmds.optionMenu('SLiB_cb_SubCat', q=1, ni=1) == 0:
                destinationDir = gib('library') + gib('mainCat') + '/' + gib('cat')
            if cmds.optionMenu('SLiB_cb_SubCat', q=1, ni=1) != 0:
                if gib('subCat') == 'Select...':
                    destinationDir = gib('library') + gib('mainCat') + '/' + gib('cat')
                else:
                    destinationDir = gib('library') + gib('mainCat') + '/' + gib('cat') + '/_SUB/' + gib('subCat')
            for r in range(0, len(copyFolders)):
                if copyFolders[r] != destinationDir + '/' + copyFiles[r]:
                    shutil.copytree(copyFolders[r], destinationDir + '/' + copyFiles[r])
                    path = destinationDir + '/' + copyFiles[r]
                    SLiBTexPathChangeWarning(path)
                    print path
                else:
                    SLiBMessager('Cannot COPY and PASTE to the same Location! Use DUPLICATE!', 'red')
                    sys.exit()

        SLiBMessager('Pasted!', 'green')
        cmds.evalDeferred(lambda: SLiBBrowserUpdateShader())

    else:
        if gib('mainCat') == 'textures':
            if copyFolder == SLiBCurrLoc() + '/' + copyFile + copyExt:
                SLiBMessager('Cannot COPY and PASTE to the same Location! Use DUPLICATE!', 'red')
                sys.exit()
            else:
                shutil.copy(copyFolder, SLiBCurrLoc() + '/' + copyFile + copyExt)
                cmds.evalDeferred(lambda: SLiBBrowserUpdateShader())
                SLiBMessager('Pasted!', 'green')
        else:
            if cmds.optionMenu('SLiB_cb_SubCat', q=1, ni=1) == 0:
                destinationDir = gib('library') + gib('mainCat') + '/' + gib('cat')
            if cmds.optionMenu('SLiB_cb_SubCat', q=1, ni=1) != 0:
                if gib('subCat') == 'Select...':
                    destinationDir = gib('library') + gib('mainCat') + '/' + gib('cat')
                else:
                    destinationDir = gib('library') + gib('mainCat') + '/' + gib('cat') + '/_SUB/' + gib('subCat')

            if copyFolder != destinationDir + '/' + copyFile:
                shutil.copytree(copyFolder, destinationDir + '/' + copyFile)
                path = destinationDir + '/' + copyFile
                SLiBTexPathChangeWarning(path)
                SLiBMessager('Pasted!', 'green')
                cmds.evalDeferred(lambda: SLiBBrowserUpdateShader())
            else:
                SLiBMessager('Cannot COPY and PASTE to the same Location! Use DUPLICATE!', 'red')
                sys.exit()

def SLiBTexPathChangeWarning(path):
    if os.path.isdir(path + '/' + 'Tex') == True:
        print 'SLiB >>> Tex folder found!'
        if cmds.menuItem('AutoRepath', q=1, cb=1):
            answer = 'Yes'
        else:
            answer = cmds.confirmDialog( title='Warning', message='You renamed, moved or duplicated a Shader / Asset with Textures.\nDo you want SLiB Browser PRO to automatically repath them?\n( Opens new scene! )', ma='center', button=['Yes','No'], defaultButton='Yes', cancelButton='No', dismissString='No' )

        if answer == 'Yes':
            for file in os.listdir(path):
                if os.path.splitext(file)[1] == '.ma' or os.path.splitext(file)[1] == '.mb' or os.path.splitext(file)[1] == '.obj':
                    cmds.file(path + '/' + file, open = 1, force = 1)

                    texFolder = path + '/' + 'Tex'
                    texlist = cmds.ls(type='file')
                    if getRenderEngine() == 'redshift':
                        texlistN = cmds.ls(type='RedshiftNormalMap')

                    for i in texlist:
                        fileName = cmds.getAttr("%s.fileTextureName" %i)
                        justName = fileName.split("/")[-1]
                        repath = texFolder + '/' + justName
                        finalPath = '${SLiBLib}' + repath.split('lib')[1]
                        cmds.setAttr("%s.fileTextureName" %i, finalPath, type="string")

                    if getRenderEngine() == 'redshift':
                        for n in texlistN:
                            fileNameN = cmds.getAttr("%s.tex0" %n)
                            justNameN = fileNameN.split("/")[-1]
                            repathN = texFolder + '/' + justNameN
                            finalPathN = '${SLiBLib}' + repathN.split('lib')[1]
                            cmds.setAttr("%s.fileTextureName" %i, finalPathN, type="string")

                    SLiBMessager('Repathing done!', 'green')

                    cmds.file(rename= path + '/' + file)
                    cmds.file(save=1)
                    cmds.file( f=1, new=1 )

        else:
            sys.exit()
    else:
        print 'SLiB >>> No Tex folder found!'

#BROWSER DOCK
def SLiBBrowserDockedUI():
    if cmds.menuItem('dockMenu', q=1, cb=1) == 1:
        if cmds.dockControl('slBrowserDock', q=1, ex=1):
            cmds.deleteUI('slBrowserDock')

        mainWindow = cmds.paneLayout(p=mel.eval('$temp1=$gMainWindow'))
        cmds.dockControl('slBrowserDock', a='right', l='SLiB Browser Pro v1.42', con=mainWindow, aa=['right', 'left'])
        cmds.control(slBrowserUI, e=1, p=mainWindow)
    if cmds.menuItem('dockMenu', q=1, cb=1) == 0:
        cmds.dockControl('slBrowserDock', e=1, fl=1)

#BROWSER DOCK 2017
def SLiBBrowserWorkspaceControl():
    if cmds.workspaceControl(WorkspaceName, ex=1):
        cmds.deleteUI(WorkspaceName)

    global slBrowserUI
    slBrowserUI = cmds.loadUI(f=SLiBGuiPath + 'SLiBBrowser.ui')
    cmds.workspaceControl(WorkspaceName, l='SLiB Browser Pro v1.42' )
    cmds.control(slBrowserUI, e=1, p=WorkspaceName)
    SLiBBrowserUI()

#BROWSER UI
def SLiBBrowserUI():
    try:
        os.mkdir(mel.eval('getenv SLiBLib;') + '/' +  'Writable')
        cmds.sysFile(mel.eval('getenv SLiBLib;') + '/' +  'Writable', red=1)
    except:
        cmds.confirmDialog(m='Could not write to specified Library folder: \n' + mel.eval('getenv SLiBLib;') + '\nEither change your permissions or run Maya as Administrator')
        sys.exit()

    if cmds.window('zoomWin', ex=1):
        cmds.deleteUI('zoomWin')

    if cmds.iconTextRadioCollection('mainCatCollection', q=1, ex=1):
        mainCatItem = cmds.iconTextRadioCollection( 'mainCatCollection', q=1, cia=1)
        for e in mainCatItem:
            cmds.deleteUI(e)
        cmds.deleteUI('mainCatCollection')

    cleanList=['slAssetCollection','Load','dock','Save','File','importREF','Import','ExportOptions','exportFRZ','exportPIV','exportMA','exportMB','exportOBJ','exportTEX','Export','CollectTex','FixTex','SearchLoc','SNR','EditDic','Tools','MassExport','GenThumbs','Batch','SLiB_cb_Cat','SLiB_cb_SubCat']
    for e in cleanList:
        try:
            cmds.deleteUI(e)
            str(e) + ' deleted'
        except:
            str(e) + ' not found'
            pass

    mel.eval("optionVar -iv inViewMessageEnable true;")

    if MAYA_VERSION != '2017':
        if cmds.window('SLiBBrowserUI', q=1, ex=1):
            cmds.deleteUI('SLiBBrowserUI')

        global slBrowserUI
        slBrowserUI = cmds.loadUI(f = SLiBGuiPath + 'SLiBBrowser.ui')

    cmds.setParent(slBrowserUI)
    cmds.iconTextCheckBox('ipt', mw=0, mh=0, w=32, h=32, onc=lambda *args: cmds.layout('SLiBIPTMainLayout', e=1, vis=1), ofc=lambda *args: (cmds.layout('SLiBIPTMainLayout', e=1, vis=0), SLiBiptOff()), i=SLiBImage+'slib_ipt_off.png', si=SLiBImage+'slib_ipt_on.png', ann=' IPT ON /OFF ', p='importOptions1')
    cmds.iconTextButton('freezeTrans', mw=0, mh=0, w=32, h=32, c=lambda *args: SLiBFreeze(), i=SLiBImage+'slib_freezetransform.png', ann=' Freeze Transformations ', p='exportTools1')
    cmds.iconTextButton('autoPlacePivot', mw=0, mh=0, w=32, h=32, c=lambda *args: SLiBAutoPLacePivot(), i=SLiBImage+'slib_autoplacepivot.png', ann=' Move Object to Origin and Pivot to Bottom ', p='exportTools2')
    cmds.iconTextCheckBox('SLiBIconCaption', mw=0, mh=0, w=32, h=32, i=SLiBImage + 'slib_ic_off.png', si=SLiBImage + 'slib_ic_on.png', cc=lambda *args: SLiBBrowserUpdateShader(), v=1, ann=' Icon Caption ON / OFF ', p='SLiBIconCaption_Layout')
    cmds.iconTextButton('SLiBRefresh', mw=0, mh=0, w=32, h=32, i=SLiBImage + 'slib_fresh_on.png', c=lambda *args: SLiBBrowserUpdateShader(), ann=' Refresh Previews ', p='SLiBIconCaption_Layout')
    cmds.iconTextCheckBox('SLiBIconMark', mw=0, mh=0, w=32, h=32, i=SLiBImage + 'slib_mark_off.png', si=SLiBImage + 'slib_mark_on.png', p='SLiBIconCaption_Layout')
    cmds.popupMenu(parent="SLiBIconMark", ctl=0, button=3)
    cmds.menuItem(l='Mark All', c=lambda *args: SLiBBrowserMark('all'))
    cmds.menuItem(l='Mark None', c=lambda *args: SLiBBrowserMark('none'))
    cmds.popupMenu(parent="SLiBRefresh", ctl=0, button=3)
    cmds.menuItem(l='Auto Fit Columns', c=lambda *args: SLiBAutoColumns())
    cmds.textField('SLiBThumbSizeComboBox', e=1, changeCommand = lambda *args: SLiBBrowserUpdateShader())
    cmds.popupMenu(parent="SLiBThumbSizeComboBox", ctl=0, button=3)
    cmds.menuItem(l='64', c=lambda *args: (cmds.textField('SLiBThumbSizeComboBox', e=1, tx='64'), SLiBBrowserUpdateShader()))
    cmds.menuItem(l='128', c=lambda *args: (cmds.textField('SLiBThumbSizeComboBox', e=1, tx='128'), SLiBBrowserUpdateShader()))
    cmds.menuItem(l='196', c=lambda *args: (cmds.textField('SLiBThumbSizeComboBox', e=1, tx='196'), SLiBBrowserUpdateShader()))
    cmds.menuItem(l='256', c=lambda *args: (cmds.textField('SLiBThumbSizeComboBox', e=1, tx='256'), SLiBBrowserUpdateShader()))
    cmds.menuItem(l='384', c=lambda *args: (cmds.textField('SLiBThumbSizeComboBox', e=1, tx='384'), SLiBBrowserUpdateShader()))
    cmds.menuItem(l='512', c=lambda *args: (cmds.textField('SLiBThumbSizeComboBox', e=1, tx='512'), SLiBBrowserUpdateShader()))
    cmds.popupMenu(parent="SLiBThumbColumnsComboBox", ctl=0, button=3)
    cmds.menuItem(l='1', c=lambda *args: (cmds.textField('SLiBThumbColumnsComboBox', e=1, tx='1'), SLiBBrowserUpdateShader()))
    cmds.menuItem(l='2', c=lambda *args: (cmds.textField('SLiBThumbColumnsComboBox', e=1, tx='2'), SLiBBrowserUpdateShader()))
    cmds.menuItem(l='3', c=lambda *args: (cmds.textField('SLiBThumbColumnsComboBox', e=1, tx='3'), SLiBBrowserUpdateShader()))
    cmds.menuItem(l='4', c=lambda *args: (cmds.textField('SLiBThumbColumnsComboBox', e=1, tx='4'), SLiBBrowserUpdateShader()))
    cmds.menuItem(l='5', c=lambda *args: (cmds.textField('SLiBThumbColumnsComboBox', e=1, tx='5'), SLiBBrowserUpdateShader()))
    cmds.menuItem(l='6', c=lambda *args: (cmds.textField('SLiBThumbColumnsComboBox', e=1, tx='6'), SLiBBrowserUpdateShader()))
    cmds.menuItem(l='7', c=lambda *args: (cmds.textField('SLiBThumbColumnsComboBox', e=1, tx='7'), SLiBBrowserUpdateShader()))
    cmds.menuItem(l='8', c=lambda *args: (cmds.textField('SLiBThumbColumnsComboBox', e=1, tx='8'), SLiBBrowserUpdateShader()))
    cmds.menuItem(l='9', c=lambda *args: (cmds.textField('SLiBThumbColumnsComboBox', e=1, tx='9'), SLiBBrowserUpdateShader()))
    cmds.menuItem(l='10', c=lambda *args: (cmds.textField('SLiBThumbColumnsComboBox', e=1, tx='10'), SLiBBrowserUpdateShader()))

    cmds.optionMenu('SLiB_cb_Cat', changeCommand = lambda *args: SLiBBrowserUpdateShader(), p='CatLayout')
    cmds.optionMenu('SLiB_cb_SubCat', changeCommand = lambda *args: SLiBBrowserUpdateShader(), p='SubCatLayout')

    cmds.optionMenu('SLiB_cb_Cat', e=1, changeCommand = lambda *args: SLiBBrowserUpdateShader())
    cmds.iconTextButton('RenderViewButton', w=256, h=256, mh=0, mw=0, image=SLiBImage + 'browser_logo.png', dcc=lambda *args: zoomWinOn(), c=lambda *args: zoomWinOff(), p='rv_holder')
    cmds.popupMenu(parent="RenderViewButton", ctl=0, button=3)
    cmds.menuItem(l='Replace Preview Image', c=lambda *args: SLiBReplacePreview())
    cmds.menuItem(d=1)
    cmds.menuItem(l='Reset', c=lambda *args: cmds.iconTextButton('RenderViewButton', e=1, i=SLiBImage + 'browser_logo.png'))
    cmds.popupMenu(parent="SLiB_TEXTFIELD_Name", ctl=0, button=3)
    cmds.menuItem(l='Name from Selection', c=lambda *args: SLiBNameFromSelection() )
    cmds.menuItem( d=1 )
    cmds.menuItem(l='Name > ShaderName', c=lambda *args: SLiBSNRSpeed() )
    cmds.iconTextCheckBox('searchSwitch', mw=0, mh=0, w=32, h=32, i=SLiBImage + 'slib_search_off.png', si=SLiBImage + 'slib_search_on.png', onc=lambda *args: SLiBBrowserUpdateShader(), ofc=lambda *args: SLiBSearchOff(), p='SLiB_BUTTON_Search')
    cmds.popupMenu(parent="SLiB_TEXTFIELD_Info", ctl=0, button=3)
    cmds.menuItem(l='Replace Notes', c=lambda *args:  SLiBReplaceNotes() )
    cmds.menuItem( d=1 )
    cmds.menuItem(l='Clear', c=lambda *args: cmds.scrollField('SLiB_TEXTFIELD_Info', e=1, tx=''))
    cmds.textField('SLiB_TEXTFIELD_Search', e=1, cc=lambda *args: SLiBSearchOn(), ec=lambda *args: SLiBSearchOn(), aie=1)
    cmds.popupMenu(parent="SLiB_TEXTFIELD_Search", ctl=0, button=3)
    cmds.menuItem(l='Clear', c=lambda *args: (cmds.textField('SLiB_TEXTFIELD_Search', e=1, tx=''), SLiBBrowserUpdateShader() ))
    cmds.textField('SLiBThumbColumnsComboBox', e=1, cc=lambda *args: SLiBBrowserUpdateShader() )

    cmds.popupMenu(parent='Cat_Layout', ctl=0, button=3)
    cmds.menuItem(l='Open in FileBrowser', c=lambda *args: SLiBOpenCatInFileBrowser() )

    cmds.popupMenu(parent='SubCat_Layout', ctl=0, button=3)
    cmds.menuItem(l='Open in FileBrowser', c=lambda *args: SLiBOpenSubCatInFileBrowser() )

    cmds.menu(l='File', allowOptionBoxes=0)
    cmds.menuItem('Load', l='Load Preview Image Scene...', c=lambda *args: loadTestRoom() )

    if MAYA_VERSION != '2017':
        cmds.menuItem( d=1 )
        cmds.menuItem('dockMenu', l='Dock Browser Window', cb=0, c=lambda *args: SLiBBrowserDockedUI() )

    cmds.menuItem( d=1 )
    cmds.menuItem(l='Duplicate', c=lambda *args: SLiBBrowserDuplicate() )
    cmds.menuItem(l='Copy', c=lambda *args: SLiBBrowserCopy() )
    cmds.menuItem(l='Paste', c=lambda *args: SLiBBrowserPaste() )
    cmds.menuItem( d=1 )
    cmds.menuItem(l='Delete', c=lambda *args: SLiBBrowserDelete() )
    cmds.menuItem('QuickDelete', l='  Delete Confirmation ', cb=0)
    cmds.menuItem( d=1 )
    cmds.menuItem('AutoRepath', l='Auto Repath Textures', cb=1)
    cmds.menuItem( d=1 )
    cmds.menuItem('Save', l='Save Window Settings', c=lambda *args: SLiBSaveWindowSettings())

    cmds.menu('Import', l='Import', allowOptionBoxes=1)
    cmds.menuItem('importREF', l='as Reference', cb=0)
    cmds.menuItem( d=1 )
    cmds.menuItem('ReUseAsset', l='Reuse Existing Asset', cb=0)
    cmds.radioMenuItemCollection('ReuseAssetOptions')
    cmds.menuItem('ReUseDuplicate', l='  Duplicate', rb=0)
    cmds.menuItem('ReUseInstance', l='  Instance', rb=1)
    cmds.menuItem( d=1 )
    cmds.menuItem('ReUseShader', l='Reuse Existing Shader', cb=1)
    cmds.menuItem( d=1 )
    cmds.menuItem('AutoREL', l='Auto REL > ABS', cb=1)

    cmds.menu('Export', l='Export', allowOptionBoxes=1)
    cmds.menuItem('exportHIS', l='Delete History', cb=0)
    cmds.menuItem('exportFRZ', l='Freeze Transform', cb=0)
    cmds.menuItem('exportPIV', l='Auto Place Pivot', cb=0)
    cmds.menuItem( d=1 )
    cmds.radioMenuItemCollection('ExportOptions')
    cmds.menuItem('exportMA', l='MA', rb=1, c=lambda *args: SLiBswitchExportButton() )
    cmds.menuItem('exportMB', l='MB', rb=0, c=lambda *args: SLiBswitchExportButton() )
    cmds.menuItem('exportOBJ', l='OBJ', rb=0, c=lambda *args: SLiBswitchExportButton() )
    cmds.menuItem( d=1 )
    cmds.menuItem('exportTEX', l='with Texture(s)', cb=1, c=lambda *args: SLiBswitchExportButton() )
    cmds.radioMenuItemCollection('ExportPath')
    cmds.menuItem('exportREL', l='  Relative Tex Path', c=lambda *args: SLiBswitchExportButton(), rb=0)
    cmds.menuItem('exportABS', l='  Absolute Tex Path', c=lambda *args: SLiBswitchExportButton(), rb=1)

    cmds.menu('Tools', l='Tools', allowOptionBoxes=0 )
    cmds.menuItem('TextureTools', l='Texture Tools Win', c=lambda *args: SLiBTTWin() )
    cmds.menuItem( d=1 )
    cmds.menuItem('SNR', l='Shading Network Renamer', c=lambda *args: SLiBSNRenamer() )
    cmds.menuItem('EditDic', l='Edit Dictionary', c=lambda *args: webbrowser.open(mel.eval('getenv SLiBLib;') +'settings/SNR_Dictionary.txt'))
    cmds.menuItem( d=1 )
    cmds.radioMenuItemCollection('searchCollection')
    cmds.menuItem('searchCat', l='Search in Category only', rb=0)
    cmds.menuItem('searchLib', l='Search in whole Library', rb=1)

    cmds.menu('Batch', l='Batch', allowOptionBoxes=0 )
    cmds.menuItem('MassExport', l='Mass Export from Folder...', c=lambda *args: SLiBMassExport() )
    cmds.menuItem( d=1 )
    cmds.menuItem('GenThumbs', l='Generate Thumbs for Textures...', c=lambda *args: SLiBBatchTextures() )

    cmds.iconTextRadioCollection( 'mainCatCollection')
    cmds.iconTextRadioButton('SHADER', st='textOnly', l='SHADER', bgc=[0,0,0], p='SLiB_AssetType', cc=lambda *args: SLiBLChangeType() )
    cmds.iconTextRadioButton('OBJECTS', st='textOnly', l='OBJECTS', bgc=[0,0,0], p='SLiB_AssetType', cc=lambda *args: SLiBLChangeType() )
    cmds.iconTextRadioButton('LIGHTS', st='textOnly', l='LIGHTS', bgc=[0,0,0], p='SLiB_AssetType', cc=lambda *args: SLiBLChangeType() )
    cmds.iconTextRadioButton('TEXTURES', st='textOnly', l='TEXTURES', bgc=[0,0,0], p='SLiB_AssetType', cc=lambda *args: SLiBLChangeType() )
    cmds.iconTextRadioButton('SHADER', e=1, sl=1)

    cmds.iconTextButton('SLiB_BUTTON_Plus', mw=0, mh=0, w=32, h=32, c=lambda *args: SLiBBrowserCreateDir(), i=SLiBImage+'slib_plus_square.png', p='SLiB_CategoryBTNs', ann=' Add Category ')
    cmds.iconTextButton('SLiB_BUTTON_Minus', mw=0, mh=0, w=32, h=32, c=lambda *args: SLiBBrowserRemoveDir(), i=SLiBImage+'slib_minus_square.png', p='SLiB_CategoryBTNs', ann=' Remove Category')
    cmds.iconTextButton('SLiB_SUBBUTTON_Plus', mw=0, mh=0, w=32, h=32, c=lambda *args: SLiBBrowserCreateSubDir(), i=SLiBImage+'slib_plus_square.png', p='SLiB_SubCategoryBTNs', ann=' Add Sub-Category ')
    cmds.iconTextButton('SLiB_SUBBUTTON_Minus', mw=0, mh=0, w=32, h=32, c=lambda *args: SLiBBrowserRemoveSubDir(), i=SLiBImage+'slib_minus_square.png', p='SLiB_SubCategoryBTNs', ann=' Remove Sub-Category')

    cmds.iconTextButton('SLiB_BUTTON_CreatePreview', c=lambda *args: SLiBCreatePreview(), w=32, h=32, mh=0, mw=0, i=SLiBImage + 'slib_createpreview.png', p='SLiB_RenderButtonLayout1', ann=' Generate Preview Image ')
    cmds.iconTextButton('SLiB_BUTTON_Render', c=lambda *args: SLiBBrowserRender(), w=32, h=32, mh=0, mw=0, i=SLiBImage + 'slib_render.png', p='SLiB_RenderButtonLayout1', ann=' Render Preview Image ')
    cmds.popupMenu(parent="SLiB_BUTTON_Render", ctl=0, button=3)
    cmds.menuItem('SetupRenderCam', l='Create RenderCam', c=lambda *args:  SLiBBrowserSetUpRendercam() )

    if getRenderEngine() == 'redshift':
        cmds.menuItem( d=1 )
        cmds.menuItem(l='Reload (Redshift)', c=lambda *args:  SLiBBrowserReloadRender() )

    cmds.iconTextButton(c=lambda *args: SLiBBrowserPlayBlast(), w=32, h=32, mh=0, mw=0, i=SLiBImage + 'slib_playblast.png', p='SLiB_RenderButtonLayout1', ann=' Playblast Preview Image ')
    cmds.iconTextButton(c=lambda *args: SLiBLoadFromRV(), w=32, h=32, mh=0, mw=0, i=SLiBImage + 'slib_renderview.png', p='SLiB_RenderButtonLayout1', ann=' Grab Preview Image from RenderView ')
    cmds.iconTextButton(c=lambda *args: SLiBLoadFromFile(), w=32, h=32, mh=0, mw=0, i=SLiBImage + 'slib_fileload.png', p='SLiB_RenderButtonLayout1', ann=' Load Preview Image from Disk ')
    cmds.iconTextCheckBox('RVLocked', w=32, h=32, mh=0, mw=0, v=0, i=SLiBImage + 'slib_unlocked.png', si=SLiBImage + 'slib_locked.png', p='SLiB_RenderButtonLayout2', ann=' Lock Preview Image ')

    cmds.layout('SLiBIPTMainLayout', e=1, vis=0, bgc=[0.2,0.2,0.2])

    #cmds.iconTextButton('IPTTargetBTN', st='textOnly', l='SET', w=64, h=24, bgc=[0.1,0.1,0.1], c=lambda *args: cmds.textField('Targets', e=1, tx=' '.join(cmds.ls(sl=1))), p='SLiBIPTLayoutOptionsR')
    #cmds.iconTextCheckBox('IPTppWinBTN', w=32, h=32, mh=0, mw=0, v=0, i=SLiBImage + 'slib_ipt_win_on.png', si=SLiBImage + 'slib_ipt_win_on.png', p='SLiBIPTLayout03L', ann=' IPT Post Placement Window ')

    cmds.iconTextCheckBox('IPTPlaceBTN', i=SLiBImage + 'slib_ipt_place.png', si=SLiBImage + 'slib_ipt_place_on.png', mw=0, mh=0, w=64, h=64, onc=lambda *args: (cmds.iconTextCheckBox('IPTPaintBTN', e=1, v=0), cmds.iconTextCheckBox('IPTMoveBTN', e=1, v=0), SLiBiptOn()), ofc=lambda *args: SLiBiptOff(), p='SLiBIPTLayout04L', ann=' IPT Place Mode ')
    cmds.iconTextCheckBox('IPTMoveBTN', i=SLiBImage + 'slib_ipt_move.png', si=SLiBImage + 'slib_ipt_move_on.png', mw=0, mh=0, w=64, h=64, onc=lambda *args: (cmds.iconTextCheckBox('IPTPlaceBTN', e=1, v=0), cmds.iconTextCheckBox('IPTPaintBTN', e=1, v=0), SLiBiptOn()), ofc=lambda *args: SLiBiptOff(), p='SLiBIPTLayout04L', ann=' IPT Move Mode ')
    cmds.iconTextCheckBox('IPTPaintBTN', i=SLiBImage + 'slib_ipt_paint.png', si=SLiBImage + 'slib_ipt_paint_on.png', mw=0, mh=0, w=64, h=64, onc=lambda *args: (cmds.iconTextCheckBox('IPTPlaceBTN', e=1, v=0), cmds.iconTextCheckBox('IPTMoveBTN', e=1, v=0), SLiBiptOn()), ofc=lambda *args: SLiBiptOff(), p='SLiBIPTLayout04L', ann=' IPT Paint Mode ')
    cmds.iconTextButton('IPTPaintBTN', i=SLiBImage + 'slib_ipt_trash.png', mw=0, mh=0, w=64, h=64, c=lambda *args: SLiBTrashLast(impObject), p='SLiBIPTLayout04L', ann=' Trash Last ')
    #cmds.iconTextButton('IPTPStopBTN', st='textOnly', l='STOP', w=64, h=64, mw=0, mh=0, c=lambda *args: (SLiBiptOff(), SLiBMessager('IPT Stopped!', 'green')), p='SLiBIPTLayout04L')


    cmds.floatSliderGrp('IPTDistSL', l='RAD', f=1, h=36, bgc=[0.2,0.2,0.2], cal=(1, 'center'), cw=[(1, 32), (2, 32)], adj=3, min=0.1, max=50, v=1, fmn=0.1, fmx=1000, step=1, pre=2, p='SLiBIPTLayout03R')

    #cmds.iconTextCheckBox('IPTAlignCB', st='textOnly', l='ALIGN', w=63, h=24, bgc=[0.15,0.15,0.15], p='SLiBIPTLayoutOptionsL')
    #cmds.text(l=' ', w=2, p='SLiBIPTLayoutOptionsL' )
    cmds.iconTextCheckBox('IPTppWinCB', st='textOnly', l='PPWin', h=24, w=32, ofc=lambda *args: SLiBIPTWinClose(), bgc=[0.15,0.15,0.15], p='SLiBIPTLayoutOptionsL')
    cmds.iconTextCheckBox('IPTRandWinBTN', st='textOnly', l='RAND', h=24, w=32, onc=lambda *args: SLiBMessager('in Development', 'yellow'), ofc=lambda *args: SLiBMessager('Coming soon!', 'yellow'), bgc=[0.125,0.125,0.125], p='SLiBIPTLayoutOptionsL')

    #cmds.iconTextCheckBox('IPTRandWinBTN', st='textOnly', l='RAND', h=24, w=32, onc=lambda *args: cmds.layout('SLiBIPTLayoutRand', e=1, vis=1), ofc=lambda *args: cmds.layout('SLiBIPTLayoutRand', e=1, vis=0), bgc=[0.125,0.125,0.125], p='SLiBIPTLayoutOptionsL')
    #cmds.iconTextButton('IPTTargetBTN', st='textOnly', l='SET', h=24, w=64, bgc=[0.1,0.1,0.1], c=lambda *args: cmds.textField('Targets', e=1, tx=' '.join(cmds.ls(sl=1))), p='SLiBIPTLayoutOptionsL')

    #cmds.textField('Targets', tx='Target Object(s)', h=24, bgc=[0,0,0], p='SLiBIPTLayoutOptionsR')
    #cmds.iconTextButton('IPTRandRotBTN', st='textOnly', l='RAND ROT', w=64, h=32, bgc=[0.2,0.2,0.2], p='SLiBIPTLayoutRand03R')
    #cmds.iconTextButton('IPTRandSclBTN', st='textOnly', l='RAND SCL', w=64, h=32, bgc=[0.2,0.2,0.2], p='SLiBIPTLayoutRand03R')

    #cmds.text(l='Randomisation Settings', h=21, bgc=[0.2,0.2,0.2], p='SLiBIPTLayout04R')

    cmds.layout('SLiBIPTLayoutRand', e=1, vis=0, bgc=[0.2,0.2,0.2])
    cmds.floatSliderGrp('IPTRandROTxMIN', f=1, h=24, bgc=[0.8,0.2,0.2], cal=(1, 'center'), cw=(1, 45), adj=2, min=0.1, max=50, v=1, fmn=0.1, fmx=1000, step=1, pre=1, p='SLiBIPTLayoutRand')
    cmds.floatSliderGrp('IPTRandROTxMAX', f=1, h=24, bgc=[0.8,0.2,0.2], cal=(1, 'center'), cw=(1, 45), adj=2, min=0.1, max=50, v=1, fmn=0.1, fmx=1000, step=1, pre=1, p='SLiBIPTLayoutRand')
    cmds.floatSliderGrp('IPTRandROTyMIN', f=1, h=24, bgc=[0.2,1,0.2], cal=(1, 'center'), cw=(1, 45), adj=2, min=0.1, max=50, v=1, fmn=0.1, fmx=1000, step=1, pre=1, p='SLiBIPTLayoutRand')
    cmds.floatSliderGrp('IPTRandROTyMAX', f=1, h=24, bgc=[0.2,1,0.2], cal=(1, 'center'), cw=(1, 45), adj=2, min=0.1, max=50, v=1, fmn=0.1, fmx=1000, step=1, pre=1, p='SLiBIPTLayoutRand')
    cmds.floatSliderGrp('IPTRandROTzMIN', f=1, h=24, bgc=[0.2,0.2,1], cal=(1, 'center'), cw=(1, 45), adj=2, min=0.1, max=50, v=1, fmn=0.1, fmx=1000, step=1, pre=1, p='SLiBIPTLayoutRand')
    cmds.floatSliderGrp('IPTRandROTzMAX', f=1, h=24, bgc=[0.2,0.2,1], cal=(1, 'center'), cw=(1, 45), adj=2, min=0.1, max=50, v=1, fmn=0.1, fmx=1000, step=1, pre=1, p='SLiBIPTLayoutRand')

    #cmds.floatFieldGrp('IPTRandSCL', h=21 ,nf=3, l='SCALE:', bgc=[0.2,0.2,0.2], cal=(1, 'center'), cw=(1, 64), value1=0.3, value2=0.5, value3=0.1, p='SLiBIPTLayout06R')

    #cmds.iconTextCheckBox('IPT_AlignBTN', st='textOnly', l='Align', w=64, h=24, bgc=[0.1,0.1,0.1], p='SLiBIPTLayout02')
    #cmds.iconTextCheckBox('IPT_PPWinBTN', st='textOnly', l='PPWin', w=64, h=24, bgc=[0.1,0.1,0.1], p='SLiBIPTLayout02')

    #cmds.floatSlider('IPTDistSL', e=1, v=cmds.textField('IPTDistTF', q=1, v=1))
    #cmds.textField('IPTDistTF', e=1, v=cmds.floatSlider('IPTDistSL', q=1, v=1))

    #cmds.checkBox('IPT_AlignCB', h=32, l='Align', bgc=[0.1,0.1,0.1], p='SLiBIPTLayout02')
    #cmds.checkBox('IPT_PPWinCB', h=32, l='PPWin', bgc=[0.1,0.1,0.1], p='SLiBIPTLayout02')

    if MAYA_VERSION != '2017':
        cmds.showWindow('SLiBBrowserUI')

    try:
        SLiBLoadWindowSettings()
    except:
        pass

    SLiBBrowserUpdateType()
    SLiBswitchExportButton()
    print 'SLiB >>> SLiB Browser PRO loaded\n',
    if MAYA_VERSION != '2017':
        if cmds.menuItem('dockMenu', q=1, cb=1):
            print 'SLiB >>> SLiB Browser PRO Window docked\n',
    SLiBMessager('SLiB Browser PRO loaded', 'green')

    if getRenderEngine() == 'vray':
        try:
            mel.eval('unifiedRenderGlobalsWindow')
            cmds.window('unifiedRenderGlobalsWindow', e=1, vis=0)
            print 'Render Settings Window initialized'
        except:
            pass

def SLiBLChangeType():
    SLiBBrowserUpdateTypeOnly()
    try:
        SLiBLoadCat()
    except:
        pass
    SLiBBrowserUpdateSubTypeOnly()
    try:
        SLiBLoadSubCat()
    except:
        pass
    SLiBBrowserUpdateShader()

def gib(x):
    if x == 'library':
        library = mel.eval('getenv SLiBLib;')
        return library
    if x == 'mainCat':
        mainCat = cmds.iconTextRadioCollection('mainCatCollection', q=1, sl=1).lower()
        return mainCat
    if x == 'cat':
        cat = cmds.optionMenu('SLiB_cb_Cat', q=1, v=1)
        return cat
    if x == 'subCat':
        subCat = cmds.optionMenu('SLiB_cb_SubCat', q=1, v=1)
        return subCat
    if x == 'file':
        try:
            file = cmds.iconTextRadioButton(cmds.iconTextRadioCollection('slAssetCollection', q=1, sl=1), q=1, l=1)
            return file
        except:
            SLiBMessager('No Shader/Asset selected!', 'red')

    if x == 'name':
        name = cmds.textField('SLiB_TEXTFIELD_Name', q=1, tx=1).replace(' ','_')
        return name
    if x == 'size':
        ObjX = abs(cmds.getAttr(selection+".boundingBoxMaxX")) + abs(cmds.getAttr(selection+".boundingBoxMinX"))
        ObjY = abs(cmds.getAttr(selection+".boundingBoxMaxY")) + abs(cmds.getAttr(selection+".boundingBoxMinY"))
        ObjZ = abs(cmds.getAttr(selection+".boundingBoxMaxZ")) + abs(cmds.getAttr(selection+".boundingBoxMinZ"))
        return ObjX,ObjY,ObjZ
    if x == 'position':
        ObjPos = cmds.xform(selection, q=1, t=1)
        return ObjPos
    if x == 'fullPath':
        fullPath = cmds.iconTextRadioButton(cmds.iconTextRadioCollection('slAssetCollection', q=1, sl=1), q=1, i=1)
        return fullPath

def SLiBCurrLoc():
    if cmds.optionMenu('SLiB_cb_SubCat', q=1, ni=1) == 0:
        currLoc = gib('library') + gib('mainCat') + '/' + gib('cat')
    if cmds.optionMenu('SLiB_cb_SubCat', q=1, ni=1) != 0:
        if gib('subCat') == 'Select...':
            currLoc = gib('library') + gib('mainCat') + '/' + gib('cat')
        else:
            currLoc = gib('library') + gib('mainCat') + '/' + gib('cat') + '/_SUB/' + gib('subCat')
    return currLoc

def SLiBSearchOn():
    if len(cmds.textField('SLiB_TEXTFIELD_Search', q=1, tx=1)) != 0:
        cmds.iconTextCheckBox('searchSwitch', e=1, v=1)
        SLiBBrowserUpdateShader()
    else:
        SLiBSearchOff()

def SLiBSearchOff():
    cmds.textField('SLiB_TEXTFIELD_Search', e=1, tx='')
    cmds.iconTextCheckBox('searchSwitch', e=1, v=0)
    SLiBBrowserUpdateShader()

def SLiBSaveCats():
    if gib('mainCat') == 'shader':
        global oldShaderCat
        oldShaderCat = gib('cat')
        if cmds.optionMenu('SLiB_cb_SubCat', q=1, ni=1) != 0:
            global oldShaderSubCat
            oldShaderSubCat = gib('subCat')
    if gib('mainCat') == 'objects':
        global oldObjectCat
        oldObjectCat = gib('cat')
        if cmds.optionMenu('SLiB_cb_SubCat', q=1, ni=1) != 0:
            global oldObjectSubCat
            oldObjectSubCat = gib('subCat')
    if gib('mainCat') == 'lights':
        global oldLightCat
        oldLightCat = gib('cat')
        if cmds.optionMenu('SLiB_cb_SubCat', q=1, ni=1) != 0:
            global oldLightSubCat
            oldLightSubCat = gib('subCat')
    if gib('mainCat') == 'textures':
        global oldTextureCat
        oldTextureCat = gib('cat')
        if cmds.optionMenu('SLiB_cb_SubCat', q=1, ni=1) != 0:
            global oldTextureSubCat
            oldTextureSubCat = gib('subCat')

def SLiBLoadCat():
    if gib('mainCat') == 'shader':
        try:
            cmds.optionMenu('SLiB_cb_Cat', e=1, v=oldShaderCat)
        except:
            cmds.optionMenu('SLiB_cb_Cat', e=1, v='Select...')
    if gib('mainCat') == 'objects':
        try:
            cmds.optionMenu('SLiB_cb_Cat', e=1, v=oldObjectCat)
        except:
            cmds.optionMenu('SLiB_cb_Cat', e=1, v='Select...')
    if gib('mainCat') == 'lights':
        try:
            cmds.optionMenu('SLiB_cb_Cat', e=1, v=oldLightCat)
        except:
            cmds.optionMenu('SLiB_cb_Cat', e=1, v='Select...')
    if gib('mainCat') == 'textures':
        try:
            cmds.optionMenu('SLiB_cb_Cat', e=1, v=oldTextureCat)
        except:
            cmds.optionMenu('SLiB_cb_Cat', e=1, v='Select...')

def SLiBLoadSubCat():
    if gib('mainCat') == 'shader':
        try:
            cmds.optionMenu('SLiB_cb_SubCat', e=1, v=oldShaderSubCat)
        except:
            pass
    if gib('mainCat') == 'objects':
        try:
            cmds.optionMenu('SLiB_cb_SubCat', e=1, v=oldObjectSubCat)
        except:
            pass
    if gib('mainCat') == 'lights':
        try:
            cmds.optionMenu('SLiB_cb_SubCat', e=1, v=oldLightSubCat)
        except:
            pass
    if gib('mainCat') == 'textures':
        try:
            cmds.optionMenu('SLiB_cb_SubCat', e=1, v=oldTextureSubCat)
        except:
            pass

def SLiBBrowserCreateDir():
    result = cmds.promptDialog(title='Create New Category', message='Enter Name:', button=['OK', 'Cancel'], defaultButton='OK', cancelButton='Cancel', dismissString='Cancel')
    if result == 'OK':
        newDirName = cmds.promptDialog(q=1, t=1).replace(' ','_')
        newDir = gib('library') + gib('mainCat') + '/' + newDirName
        if os.path.isdir(newDir) == 1:
            SLiBMessager('Category already exists!', 'red')
            sys.exit()
        else:
            cmds.sysFile( newDir, makeDir=1 )
            cmds.sysFile( newDir + '/_SUB', makeDir=1 )
            SLiBBrowserUpdateTypeOnly()
            cmds.optionMenu('SLiB_cb_Cat', e=1, v=newDirName)
            SLiBBrowserUpdateSubType()
            SLiBBrowserUpdateShader()
            SLiBMessager('Cat < ' + str(newDirName) + ' > created.', 'green')
    else:
        sys.exit()

def SLiBBrowserRemoveDir():
    if gib('mainCat') == 'textures':
        deleteDir = SLiBCurrLoc()
        cmds.sysFile( deleteDir + '/_SUB', red=1 )
        cmds.sysFile( deleteDir + '/_THUMBS', red=1 )
    else:
        deleteDir = gib('library') + gib('mainCat') + '/' + gib('cat')

    if len(os.listdir(deleteDir)) == 1:
        if os.listdir(deleteDir)[0] == '_SUB':
            cmds.sysFile( deleteDir + '/_SUB', red=1 )
            cmds.sysFile( deleteDir, red=1 )
    else:
        SLiBMessager('Category is not empty!', 'red')
        sys.exit()
    SLiBBrowserUpdateType()
    SLiBBrowserUpdateSubType()
    SLiBMessager('Cat < ' + str(deleteDir) + ' > removed.', 'green')

def SLiBBrowserCreateSubDir():
    result = cmds.promptDialog(title='Create New SubCategory', message='Enter Name:', button=['OK', 'Cancel'], defaultButton='OK', cancelButton='Cancel', dismissString='Cancel')
    if result == 'OK':
        newSubDirName = cmds.promptDialog(q=1, t=1).replace(' ','_')
        newSubDir = gib('library') + gib('mainCat') + '/' + gib('cat') + '/_SUB/' + newSubDirName
        if os.path.isdir(newSubDir) == 1:
            SLiBMessager('SubCategory already exists!', 'red')
            sys.exit()
        else:
            cmds.sysFile( newSubDir, makeDir=1 )
            SLiBBrowserUpdateSubType()
            cmds.optionMenu('SLiB_cb_SubCat', e=1, v=newSubDirName)
            SLiBBrowserUpdateShader()
            SLiBMessager('Cat < ' + str(newSubDirName) + ' > created.', 'green')
    else:
        sys.exit()

def SLiBBrowserRemoveSubDir():
    deleteSubDir = gib('library') + gib('mainCat') + '/' + gib('cat') + '/_SUB/' + gib('subCat')
    cmds.sysFile( deleteSubDir, red=1 )
    SLiBMessager('SubCat < ' + str(deleteSubDir) + ' > removed.', 'green')
    SLiBBrowserUpdateSubType()
    SLiBBrowserUpdateShader()

def SLiBReplacePreview():
    if gib('mainCat') == 'textures':
        SLiBMessager('Textures not supported!', 'red')
        sys.exit()
    else:
        if cmds.iconTextRadioCollection('slAssetCollection', q=1, sl=1) == 'NONE':
            SLiBMessager('Please select something!', 'red')
            sys.exit()
        else:
            oldOne = cmds.iconTextRadioCollection('slAssetCollection', q=1, sl=1)
            newPreview = os.path.splitext(gib('file'))[0]
            newOne = cmds.iconTextButton('RenderViewButton', q=1, i=1)
            image = om.MImage()
            image.readFromFile(newOne)
            image.resize( 512, 512 )
            image.writeToFile(newPreview + '.png', 'png')
            SLiBBrowserUpdateShader()
            cmds.iconTextRadioButton(oldOne, e=1, sl=1)
            SLiBMessager('Preview Image replaced!', 'green')

def SLiBReplaceNotes():
    if gib('mainCat') == 'textures':
        SLiBMessager('Textures not supported!', 'red')
        sys.exit()
    else:
        if cmds.iconTextRadioCollection('slAssetCollection', q=1, sl=1) == 'NONE':
            SLiBMessager('Please select something!', 'red')
            sys.exit()
        else:
            notesFile = os.path.splitext(gib('file'))[0]
            notes = cmds.scrollField('SLiB_TEXTFIELD_Info', q=1, tx=1)
            notes = notes.replace(u"\u2018", "").replace(u"\u2019", "").replace(u"\u201c","").replace(u"\u201d", "")
            f = open(notesFile + '.info', 'w')
            f.write(str(notes))
            f.close()
            SLiBMessager('Notes updated!', 'green')


def SLiBBrowserPlayBlast():
    object = cmds.ls(sl=1)
    imageName = SLiBTempStore + '/' +'icontemp'
    renderGlobals = cmds.getAttr("defaultRenderGlobals.imageFormat")
    perspPanel = cmds.getPanel( withLabel='Persp View')
    cmds.setFocus(perspPanel)
    cmds.select(cl=1)
    camera1 = cmds.modelPanel(cmds.getPanel(wf=1), q=1, cam=1)
    cmds.camera(camera1, e=1, displayResolution=0, overscan=1.0)
    playBlast = cmds.playblast(forceOverwrite = 1, framePadding = 0, viewer = 0, showOrnaments = 0, frame = cmds.currentTime(q = 1), widthHeight = [512,512], percent = 100, format = 'iff', compression = 'png', filename = imageName)
    playBlast = playBlast.replace('####', '0')
    cmds.iconTextButton('RenderViewButton', e=1, i=SLiBImage + 'browser_logo.png')
    cmds.iconTextButton('RenderViewButton', e=1, i=playBlast)
    if object != None:
        cmds.select(object)

def SLiBBrowserRender():
    try:
        initialView = omui.M3dView().active3dView()
        camDP = om.MDagPath()
        initialView.getCamera(camDP)
        camFn = om.MFnCamera(camDP)
        camera1 = cmds.modelPanel(cmds.getPanel(wf=1), q=1, cam=1)
        camera0 = cmds.listRelatives(camera1)[0]
    except:
        SLiBMessager('Please select Viewport you want to Render from!', 'red')
        sys.exit()

    if getRenderEngine() != 'vray':
        cmds.setAttr('defaultResolution.width', 512)
        cmds.setAttr('defaultResolution.height', 512)
        aspRatio = float(cmds.getAttr('defaultResolution.width'))/float(cmds.getAttr('defaultResolution.height'))
        cmds.setAttr('defaultResolution.deviceAspectRatio', aspRatio)

    SLiBBrowserStopRender()

    if getRenderEngine() == 'arnold':
        cmds.arnoldRender(cam=camera1)

    if getRenderEngine() == 'vray':
        mel.eval("vrend -ipr false;")
        cmds.setAttr('vraySettings.wi',512)
        cmds.setAttr('vraySettings.he', 512)
        aspRatio = float(cmds.getAttr('vraySettings.wi'))/float(cmds.getAttr('vraySettings.he'))
        cmds.setAttr('vraySettings.aspr', aspRatio)
        cmds.setAttr('vraySettings.vfbOn', 0)
        cmds.setAttr('vraySettings.samplerType', 1)
        cmds.setAttr ('vraySettings.sRGBOn', 1)
        mel.eval("vrend -cam "+camera0+";")

    if getRenderEngine() == 'redshift':
        cmds.setAttr("defaultRenderGlobals.imageFormat", 51)
        cmds.rsRender( r=1, cam=camera0)

    if getRenderEngine() == 'mentalRay':
        cmds.Mayatomr(pv=1, cam=camera0)

    if getRenderEngine() == 'mayaSoftware':
        mel.eval("renderIntoNewWindow render")

    imageName = SLiBTempStore + 'icontemp' + '.png'
    renderGlobals = cmds.getAttr("defaultRenderGlobals.imageFormat")
    cmds.setAttr("defaultRenderGlobals.imageFormat", 32)
    cmds.renderWindowEditor('renderView', e=1, wi=imageName)
    cmds.setAttr("defaultRenderGlobals.imageFormat", renderGlobals)
    cmds.iconTextButton('RenderViewButton', e=1, i=SLiBImage + 'browser_logo.png')
    cmds.iconTextButton('RenderViewButton', e=1, i=imageName)

    if getRenderEngine() == 'mayaSoftware':
        if cmds.window('renderViewWindow', q=1, ex=1):
            cmds.deleteUI('renderViewWindow')

def SLiBCreatePreview():
    if getRenderEngine() == 'arnold' or getRenderEngine() == 'mentalRay' or getRenderEngine() == 'redshift' or getRenderEngine() == 'vray':
        multiSel = SLiBMultiSel()
        if multiSel != None:
            for e in multiSel:
                cmds.iconTextRadioButton('icon' + e, e=1, sl=1)
                selShaderPreview = cmds.iconTextRadioButton('icon' + e, q=1, i=1)
                SLiBCreatePreviewRender(selShaderPreview)
            cmds.evalDeferred(lambda: SLiBBrowserUpdateShader())
    else:
        SLiBMessager('Unsupported renderer selected!', 'red')
        sys.exit()

def SLiBCreatePreviewRender(selShaderPreview):
    if gib('mainCat') == 'shader':
        if os.path.isfile(mel.eval('getenv SLiBLib;') + "/scene/temp_preview_mat.ma"):
            os.remove(mel.eval('getenv SLiBLib;') + "/scene/temp_preview_mat.ma")

        if os.path.isfile(mel.eval('getenv SLiBLib;') + "/scene/temp_preview_mat.mb"):
            os.remove(mel.eval('getenv SLiBLib;') + "/scene/temp_preview_mat.mb")

        selShader = gib('file')
        selShaderExt = os.path.splitext(selShader)[1]

        tempShader = mel.eval('getenv SLiBLib;') + '/scene/temp_preview_mat'
        shutil.copy(selShader, tempShader + selShaderExt)

        imageFile =  mel.eval('getenv SLiBLib;') + '/scene/'
        sceneFile =  mel.eval('getenv SLiBLib;') + '/scene/' + getRenderEngine() + '_SLiB_preview.ma'

        ## WINDOWS
        if platform.system()  == "win32" or platform.system()  == 'Windows':
            RenderComLoc = mel.eval('getenv "MAYA_LOCATION"') + '/bin'
            #if getRenderEngine() == 'mentalRay':
                #command = '"' + RenderComLoc + "/render" + '"' + " -r mr -v 5 -rd " + imageFile  + ' ' + sceneFile
            #else:
            #    command = '"' + RenderComLoc + "/render" + '"' + " -r " + getRenderEngine() + " -rd " + imageFile  + ' ' + sceneFile

            if getRenderEngine() == 'mentalRay':
                command = '"' + RenderComLoc + "/render" + '"' + " -r mr -v 5 -rd " + '"' + imageFile + '"' + ' ' + '"' + sceneFile + '"'
            else:
                command = '"' + RenderComLoc + "/render" + '"' + " -r " + getRenderEngine() + " -rd " + '"' + imageFile + '"' + ' ' + '"' + sceneFile + '"'

            batchName = (mel.eval('getenv SLiBLib;') + '/scene/RenderPreviews.bat')
            batchFile = open(batchName,"w")
            batchFile.write(command)
            batchFile.close()

            subprocess.call(command)

        ## MAC
        elif platform.system()  == "darwin" or platform.system()  == "Darwin":
            RenderComLoc = mel.eval('getenv "MAYA_LOCATION"') + '/bin'

            if getRenderEngine() == 'mentalRay':
                command = str(RenderComLoc + "/Render -r mr -v 5 -rd " + imageFile  + ' ' + sceneFile)
            else:
                command = str(RenderComLoc + "/Render -r " + getRenderEngine() + " -rd " + imageFile  + ' ' + sceneFile)

            batchName = (mel.eval('getenv SLiBLib;') + '/scene/RenderPreviews.sh')
            batchFile = open(batchName, "w")
            batchFile.write("#!/bin/bash")
            batchFile.write("\n")
            batchFile.write(str(command))
            batchFile.close()
            os.system("chmod 755 "+ batchName)
            subprocess.call(batchName)

        '''
        ## LINUX
        elif platform.system()  == "linux" or platform.system()  == "linux2":
            RenderComLoc = mel.eval('getenv "MAYA_LOCATION"') + '/bin'
            if getRenderEngine() == 'mentalRay':
                command = RenderComLoc + "/Render -r mr -v 5 -rd " + imageFile  + ' ' + sceneFile
            else:
                command = RenderComLoc + "/Render -r " + getRenderEngine() + " -rd " + imageFile  + ' ' + sceneFile
        '''

        SLiBdSwapImage(selShaderPreview)

    else:
        SLiBMessager('Only working for Shader Previews!', 'red')
        cmds.iconTextButton('RenderViewButton', e=1, i=SLiBImage + "/browser_logo.png")
        sys.exit()

def SLiBdSwapImage(selShaderPreview):
    if os.path.isfile(mel.eval('getenv SLiBLib;') + "/scene/tempPreview.png"):
        shutil.copy(mel.eval('getenv SLiBLib;') + "/scene/tempPreview.png", selShaderPreview)

def SLiBLoadFromRV():
    imageName = SLiBTempStore + 'icontemp' + '.png'
    renderGlobals = cmds.getAttr("defaultRenderGlobals.imageFormat")
    cmds.setAttr("defaultRenderGlobals.imageFormat", 32)
    cmds.renderWindowEditor('renderView', e=1, wi=imageName)
    cmds.setAttr("defaultRenderGlobals.imageFormat", renderGlobals)
    cmds.iconTextButton('RenderViewButton', e=1, i=imageName)

def SLiBLoadFromFile():
    imageName = cmds.fileDialog(m=0)
    if len(imageName) == 0:
        sys.exit()
    else:
        cmds.iconTextButton('RenderViewButton', e=1, i=imageName)

def SLiBBrowserStopRender():
    try:
        if getRenderEngine() == 'arnold':
            cmds.arnoldIpr(mode='stop')
        if getRenderEngine() == 'vray':
            mel.eval('vrayProgressEnd;')
            mel.eval("vrend -ipr false;")
        if getRenderEngine() == 'redshift':
            cmds.rsRender( r=1, stopIpr=1)
    except:
        pass

def SLiBBrowserReloadRender():
    imageName = SLiBTempStore + 'icontemp' + '.png'
    try:
        cmds.iconTextButton('RenderViewButton', e=1, i=SLiBImage + 'browser_logo.png')
        cmds.iconTextButton('RenderViewButton', e=1, i=imageName)
    except:
        cmds.iconTextButton('RenderViewButton', e=1, i=SLiBImage + 'browser_logo.png')

def SLiBSNRenamer():
    if len(cmds.ls(sl=1)) != 0:
        SG = cmds.listConnections(cmds.ls(sl=1), type="shadingEngine")
        if SG == None:
            SLiBMessager('Please select a Material!', 'red')
            sys.exit()
        else:
            MAT = cmds.ls(cmds.listConnections(SG), mat=1)
            if len(MAT) > 1:
                for m in MAT:
                    if cmds.nodeType(m) == 'displacementShader':
                        MAT.remove(m)

            cmds.select(MAT)
            snrDict=[]
            f = open( mel.eval('getenv SLiBLib;') +'settings/SNR_Dictionary.txt', 'r')
            for line in f.readlines():
                snrDict.append([line.rstrip()])

            result = cmds.promptDialog(title='Rename', message='Enter Name:', button=['OK', 'Cancel'], defaultButton='OK', cancelButton='Cancel', dismissString='Cancel')
            if result == 'OK':
                newName = cmds.promptDialog(q=1, t=1).replace(' ','_')
                for i in range(0, len(MAT)):
                    SG = cmds.listConnections(MAT[i], type='shadingEngine')
                    if SG[0] == "" or SG[0] == "initialShadingGroup":
                        mel.warning("select a material node first")
                    else:
                        cmds.select(cmds.listHistory(SG[0]), noExpand=1)
                        SNW = cmds.ls(sl=1)
                        for e in SNW:
                            nodeTyp = str(cmds.nodeType(e))
                            nodeTyp_oldList = [x[0].split(',')[0] for x in snrDict]
                            nodeTyp_newList = [x[0].split(',')[1] for x in snrDict]

                            if nodeTyp in nodeTyp_oldList:
                               entspricht = nodeTyp_oldList.index(nodeTyp)
                               cmds.rename(e, newName + nodeTyp_newList[entspricht])
    else:
        SLiBMessager('Please select a Material!', 'red')
        sys.exit()
    cmds.select(cl=1)

def SLiBswitchExportButton():
    cmds.iconTextButton('SLiB_BUTTON_Plus', e=1, en=1)
    cmds.iconTextButton('SLiB_BUTTON_Minus', e=1, en=1)
    cmds.iconTextButton('SLiB_SUBBUTTON_Plus', e=1, en=1)
    cmds.iconTextButton('SLiB_SUBBUTTON_Minus', e=1, en=1)
    cmds.textField('SLiB_TEXTFIELD_Search', e=1, en=1)
    cmds.iconTextCheckBox('searchSwitch', e=1, en=1)

    if cmds.menuItem('exportMA', q=1, rb=1) == 1:
        ext = cmds.menuItem('exportMA', q=1, l=1)
    if cmds.menuItem('exportMB', q=1, rb=1) == 1:
        ext = cmds.menuItem('exportMB', q=1,l=1)
    if cmds.menuItem('exportOBJ', q=1, rb=1) == 1:
        ext = cmds.menuItem('exportOBJ', q=1, l=1)

    if cmds.menuItem('exportTEX', q=1, cb=1) == 1:
        if cmds.iconTextRadioButton('OBJECTS', q=1, sl=1) == 1:
            if cmds.menuItem('exportREL', q=1, rb=1) == True:
                cmds.button('SLiB_BUTTON_Export', e=1, en=1, l='EXPORT ASSET (' +  str(ext) + ') + TEX (REL)', c=lambda *args: SLiBBrowserExport())
            if cmds.menuItem('exportABS', q=1, rb=1) == True:
                cmds.button('SLiB_BUTTON_Export', e=1, en=1, l='EXPORT ASSET (' + str(ext) + ') + TEX (ABS)', c=lambda *args: SLiBBrowserExport())
        if cmds.iconTextRadioButton('SHADER', q=1, sl=1) == 1:
            if cmds.menuItem('exportREL', q=1, rb=1) == True:
                cmds.button('SLiB_BUTTON_Export', e=1, en=1, l='EXPORT SHADER (' + str(ext) + ') + TEX (REL)', c=lambda *args: SLiBBrowserExport())
            if cmds.menuItem('exportABS', q=1, rb=1) == True:
                cmds.button('SLiB_BUTTON_Export', e=1, en=1, l='EXPORT SHADER (' + str(ext) + ') + TEX (ABS)', c=lambda *args: SLiBBrowserExport())
    else:
        if cmds.iconTextRadioButton('OBJECTS', q=1, sl=1) == 1:
            cmds.button('SLiB_BUTTON_Export', e=1, en=1, l='EXPORT ASSET', c=lambda *args: SLiBBrowserExport())
        if cmds.iconTextRadioButton('SHADER', q=1, sl=1) == 1:
            cmds.button('SLiB_BUTTON_Export', e=1, en=1, l='EXPORT SHADER', c=lambda *args: SLiBBrowserExport())
    if cmds.iconTextRadioButton('LIGHTS', q=1, sl=1) == 1:
        cmds.button('SLiB_BUTTON_Export', e=1, en=1, l='EXPORT LIGHT', c=lambda *args: SLiBBrowserExport())
    if cmds.iconTextRadioButton('TEXTURES', q=1, sl=1) == 1:
        cmds.button('SLiB_BUTTON_Export', e=1, en=0, l='NO EXPORT')

def getDagPath(objectName):
    tempList = om.MSelectionList()
    tempList.add(objectName)
    dagpath = om.MDagPath()
    tempList.getDagPath(0, dagpath)
    return dagpath

def getInViewObjs():
    viewObj = []
    activeView = omui.M3dView.active3dView()
    om.MGlobal.selectFromScreen(0, 0, activeView.portWidth(), activeView.portHeight(), om.MGlobal.kReplaceList)

    for e in cmds.ls(sl=1):
        cmds.select(e)
        if cmds.filterExpand(sm=12, fp=1) != None:
            viewObj.append(e)
    cmds.select(cl=1)

    if viewObj:
        return viewObj
    else:
        sys.exit()

def SLiBiptOn():
    '''
    if cmds.textField('Targets', q=1, tx=1) == 'Target Object(s)' or len(cmds.textField('Targets', q=1, tx=1)) == 0:
        SLiBMessager('Please Set Target Object(s) first!', 'red')
        cmds.evalDeferred(lambda: SLiBiptOff())
        sys.exit()
    '''

    if cmds.iconTextRadioCollection('slAssetCollection', q=1, ex=1):
        if cmds.iconTextRadioCollection('slAssetCollection', q=1, sl=1) == 'NONE':
            SLiBMessager('Please select an Asset!', 'red')
            cmds.evalDeferred(lambda: SLiBiptOff())
            sys.exit()

    if gib('mainCat') != 'objects':
        SLiBMessager('IPT is only working with Objects!', 'red')
        cmds.evalDeferred(lambda: SLiBiptOff())
        sys.exit()

    else:
        cmds.inViewMessage('IPTinview', amg=' <hl>IPT is active</hl>', pos='topCenter')
        SLiBMessager('IPT started!', 'green')
        placerShader = cmds.shadingNode('lambert', asShader=1, skipSelect=1)
        cmds.rename(placerShader, 'placerMAT')
        cmds.setAttr('placerMAT.color', 1, 0.55, 0, type='double3')
        cmds.setAttr('placerMAT.ambientColor', 0.5, 0.25, 0.25, type='double3')
        cmds.setAttr('placerMAT.transparency', 0.25, 0.25, 0.25, type='double3')

        Context = 'iptContext'
        if cmds.draggerContext(Context, q=1, ex=1) == 1:
            cmds.deleteUI(Context)

        worldUp = om.MVector(0,1,0)

        def onPress():
            targetList = getInViewObjs()
            global targetShapes
            targetShapes = []
            for t in targetList:
                shape = ''.join(cmds.listRelatives(t, s=1))
                if shape:
                    targetShapes.append(shape)

            print targetShapes #>>>>>>>>>>>>>>>>> DELETE ME LATER

            if cmds.window('SLiBTweakWin', ex=1):
                cmds.deleteUI('SLiBTweakWin')

            initPos = cmds.draggerContext(Context, q=1, ap=1)
            currentView = omui.M3dView().active3dView()
            camDP = om.MDagPath()
            currentView.getCamera(camDP)
            camFn = om.MFnCamera(camDP)
            farclip = camFn.farClippingPlane()
            worldPos, worldDir = viewToWorld(initPos[0],initPos[1])
            closestHit = None
            closestObj = None

            for obj in targetShapes:
                state, hit, fnMesh, facePtr, triPtr = intersect(obj, worldPos, worldDir, farclip)

                if state is True:
                    dif = [hit.x - worldPos[0],hit.y - worldPos[1],hit.z - worldPos[2]]
                    distToCam = math.sqrt(math.pow(float(dif[0]),2) + math.pow(float(dif[1]),2) + math.pow(float(dif[2]),2))
                    if closestHit == None:
                        closestHit = distToCam
                        closestObj = [state, hit, fnMesh, facePtr, triPtr]
                    elif distToCam < closestHit:
                        closestHit = distToCam
                        closestObj = [state, hit, fnMesh, facePtr, triPtr]

            if closestObj is not None:
                state, hit, fnMesh, facePtr, triPtr = closestObj

                if hit:

                    mHit = om.MPoint(hit)
                    normal = om.MVector() # get smooth normal
                    fnMesh.getClosestNormal(mHit, normal, om.MSpace.kWorld, None)
                    tangent = crossProduct(normal,worldUp)
                    init_pnt = om.MPoint(mHit.x, mHit.y, mHit.z)

                    if cmds.iconTextCheckBox('IPTPlaceBTN', q=1, v=1) == 1:
                        cmds.spaceLocator(n='hitLoc', p=(init_pnt.x, init_pnt.y, init_pnt.z), a=1)
                        cmds.spaceLocator(n='targetLoc', p=(init_pnt.x, init_pnt.y ,init_pnt.z), a=1)
                        cmds.xform('targetLoc', cp=1)

                    if cmds.iconTextCheckBox('IPTPaintBTN', q=1, v=1) != 1:
                        cmds.polyCube(n='Placer', h=10, w=10, d=10)

                        if os.path.isfile(os.path.splitext(gib('file'))[0] + '.dim') == 1:
                            dimensions = open(os.path.splitext(gib('file'))[0] + '.dim')
                            dim = dimensions.read().split(',')
                            cmds.polyCube('Placer', e=1, d=dim[2], h=dim[1], w=dim[0])
                            distance = float(dim[1])/2
                            cmds.move(0, distance*-1, 0, "Placer.scalePivot","Placer.rotatePivot", a=1)
                            cmds.move(0, distance, 0, "Placer", a=1)
                            cmds.makeIdentity('Placer', apply=1, t=1, r=1, s=1, n=0)

                        cmds.hyperShade('Placer', a='placerMAT')
                        cmds.setAttr('Placer.translateX', init_pnt.x)
                        cmds.setAttr('Placer.translateY', init_pnt.y)
                        cmds.setAttr('Placer.translateZ', init_pnt.z)

                    if cmds.iconTextCheckBox('IPTPlaceBTN', q=1, v=1) == 1:
                        cmds.setAttr('targetLoc.translateX', -1*init_pnt.x)
                        cmds.setAttr('targetLoc.translateY', -1*init_pnt.y)
                        cmds.setAttr('targetLoc.translateZ', -1*init_pnt.z)
                        cmds.makeIdentity('targetLoc', apply=1, t=1, r=1, s=1, n=0)
                        cmds.aimConstraint('targetLoc', 'Placer')
                        cmds.setAttr('Placer_aimConstraint1.offsetY', 90)

                    if cmds.iconTextCheckBox('IPTPaintBTN', q=1, v=1) == 1:
                        paintedObject = ''.join(SLiBBrowserImport('Normal'))

                        cmds.setAttr(paintedObject + '.translateX', init_pnt.x)
                        cmds.setAttr(paintedObject + '.translateY', init_pnt.y)
                        cmds.setAttr(paintedObject + '.translateZ', init_pnt.z)

                        rx, ry, rz = getEulerRotationQuaternion(worldUp, normal)

                        cmds.setAttr(paintedObject + '.rotateX', rx)
                        cmds.setAttr(paintedObject + '.rotateY', ry)
                        cmds.setAttr(paintedObject + '.rotateZ', rz)

                        global prevHit
                        prevHit = init_pnt

            cmds.refresh()


        def onDrag():
            dist = cmds.floatSliderGrp('IPTDistSL', q=1, v=1)

            currentPos = cmds.draggerContext(Context, q=1, dp=1)
            currentView = omui.M3dView().active3dView()
            camDP = om.MDagPath()
            currentView.getCamera(camDP)
            camFn = om.MFnCamera(camDP)
            farclip = camFn.farClippingPlane()
            worldPos, worldDir = viewToWorld(currentPos[0],currentPos[1])
            closestHit = None
            closestObj = None

            for obj in targetShapes:
                state, hit, fnMesh, facePtr, triPtr = intersect(obj, worldPos, worldDir, farclip)

                if state is True:
                    dif = [hit.x - worldPos[0],hit.y - worldPos[1],hit.z - worldPos[2]]
                    distToCam = math.sqrt(math.pow(float(dif[0]),2) + math.pow(float(dif[1]),2) + math.pow(float(dif[2]),2))

                    if closestHit == None:
                        closestHit = distToCam
                        closestObj = [state, hit, fnMesh, facePtr, triPtr]
                    elif distToCam < closestHit:
                        closestHit = distToCam
                        closestObj = [state, hit, fnMesh, facePtr, triPtr]

            if closestObj is not None:
                state, hit, fnMesh, facePtr, triPtr = closestObj

                if hit:

                    mHit = om.MPoint(hit)
                    normal = om.MVector() # get smooth normal
                    fnMesh.getClosestNormal(mHit, normal, om.MSpace.kWorld, None)
                    hit_pnt = om.MPoint(mHit.x, mHit.y, mHit.z)

                    if cmds.iconTextCheckBox('IPTPlaceBTN', q=1, v=1) == 1:
                        cmds.setAttr('targetLoc.translateX', hit_pnt.x)
                        cmds.setAttr('targetLoc.translateY', hit_pnt.y)
                        cmds.setAttr('targetLoc.translateZ', hit_pnt.z)

                    if cmds.iconTextCheckBox('IPTMoveBTN', q=1, v=1) == 1:
                        cmds.setAttr('Placer.translateX', hit_pnt.x)
                        cmds.setAttr('Placer.translateY', hit_pnt.y)
                        cmds.setAttr('Placer.translateZ', hit_pnt.z)

                        rx, ry, rz = getEulerRotationQuaternion(worldUp, normal)

                        cmds.setAttr('Placer.rotateX', rx)
                        cmds.setAttr('Placer.rotateY', ry)
                        cmds.setAttr('Placer.rotateZ', rz)

                    if cmds.iconTextCheckBox('IPTPaintBTN', q=1, v=1) == 1:
                        distance = math.sqrt( math.pow((prevHit.x-hit_pnt.x),2) + math.pow((prevHit.y-hit_pnt.y),2) + math.pow((prevHit.z-hit_pnt.z),2))

                        if distance > dist:
                            if cmds.objExists('Placer'):
                                cmds.delete('Placer')

                            paintedObject = ''.join(SLiBBrowserImport('Normal'))

                            cmds.setAttr(paintedObject + '.translateX', hit_pnt.x)
                            cmds.setAttr(paintedObject + '.translateY', hit_pnt.y)
                            cmds.setAttr(paintedObject + '.translateZ', hit_pnt.z)

                            rx, ry, rz = getEulerRotationQuaternion(worldUp, normal)

                            cmds.setAttr(paintedObject + '.rotateX', rx)
                            cmds.setAttr(paintedObject + '.rotateY', ry)
                            cmds.setAttr(paintedObject + '.rotateZ', rz)

                            global prevHit
                            prevHit = hit_pnt

                        else:
                            pass


            cmds.refresh()

        def onRelease():
            try:
                if cmds.iconTextCheckBox('IPTPaintBTN', q=1, v=1) != 1:
                    global impObject
                    impObject = ''.join(SLiBBrowserImport('Normal'))

                    posX = cmds.getAttr('Placer.translateX')
                    posY = cmds.getAttr('Placer.translateY')
                    posZ = cmds.getAttr('Placer.translateZ')

                    rotX = cmds.getAttr('Placer.rotateX')
                    rotY = cmds.getAttr('Placer.rotateY')
                    rotZ = cmds.getAttr('Placer.rotateZ')

                    scalX = cmds.getAttr('Placer.scaleX')
                    scalY = cmds.getAttr('Placer.scaleY')
                    scalZ = cmds.getAttr('Placer.scaleZ')

                    cmds.setAttr(impObject + '.translateX', posX)
                    cmds.setAttr(impObject + '.translateY', posY)
                    cmds.setAttr(impObject + '.translateZ', posZ)

                    cmds.setAttr(impObject + '.rotateX', rotX)
                    cmds.setAttr(impObject + '.rotateY', rotY)
                    cmds.setAttr(impObject + '.rotateZ', rotZ)

                    try:
                        cmds.scriptJob(kill=moveLocJob, force=1)
                        cmds.scriptJob(kill=rotateLocJob, force=1)
                        cmds.scriptJob(kill=scaleLocJob, force=1)
                    except:
                        pass

                    if cmds.objExists('hitLoc'):
                        cmds.delete('hitLoc')
                    if cmds.objExists('targetLoc'):
                        cmds.delete('targetLoc')
                    if cmds.objExists('Placer'):
                        cmds.delete('Placer')

                    if cmds.window('SLiBTweakWin', ex=1):
                        cmds.deleteUI('SLiBTweakWin')

                    if cmds.iconTextCheckBox('IPTppWinCB', q=1, v=1) == 1:
                        cmds.window('SLiBTweakWin', title="", sizeable=0)
                        cmds.rowColumnLayout('sliderWinLayout', nc=1, w=264)
                        cmds.rowColumnLayout('SLiBTweakMenuLayout', w=264, h=34, nc=7, cw=[(1, 32), (2, 32), (3, 32), (4, 32), (5, 32), (6, 32), (7, 72)], bgc=[0,0,0], p='sliderWinLayout')
                        cmds.iconTextRadioCollection('tweakTools', p='SLiBTweakMenuLayout')
                        cmds.iconTextRadioButton('IPTCtx', mw=0, mh=0, w=32, h=32, onc=lambda *args: (cmds.setToolTo('iptContext')), i=SLiBImage+'slib_tweakipt_off.png', si=SLiBImage+'slib_ipt_on.png', p='SLiBTweakMenuLayout', ann='IPT')
                        cmds.iconTextRadioButton('IPTmoveCtx', mw=0, mh=0, w=32, h=32, onc=lambda *args: (cmds.setToolTo("Move"), cmds.select(impObject)), i=SLiBImage+'slib_tweakmove_off.png', si=SLiBImage+'slib_tweakmove_on.png', p='SLiBTweakMenuLayout', ann='Move')
                        cmds.iconTextRadioButton('IPTrotateCtx', mw=0, mh=0, w=32, h=32, onc=lambda *args: (cmds.setToolTo("Rotate"), cmds.select(impObject)), i=SLiBImage+'slib_tweakrotate_off.png', si=SLiBImage+'slib_tweakrotate_on.png', p='SLiBTweakMenuLayout', ann='Rotate')
                        cmds.iconTextRadioButton('IPTscaleCtx', mw=0, mh=0, w=32, h=32, onc=lambda *args: (cmds.setToolTo("Scale"), cmds.select(impObject)), i=SLiBImage+'slib_tweakscale_off.png', si=SLiBImage+'slib_tweakscale_on.png', p='SLiBTweakMenuLayout', ann='Scale')
                        cmds.iconTextRadioCollection('tweakTools', e=1, sl='IPTCtx')

                        cmds.iconTextButton('trashLastCtx', mw=0, mh=0, w=32, h=32, c=lambda *args: SLiBTrashLast(impObject), i=SLiBImage+'slib_trash_on.png', p='SLiBTweakMenuLayout', ann='Trash last placed Object')
                        cmds.iconTextButton('TweakWinMovButton', w=32, h=32, mw=0, mh=0, c=lambda *args: cmds.window('SLiBTweakWin', e=1, mxb=1), i=SLiBImage+'slib_movetweakwin.png', p='SLiBTweakMenuLayout', ann='Move IPT Tweak Window')
                        #cmds.text(l='Increment by:', w=108, p='SLiBTweakMenuLayout')
                        cmds.optionMenu('SLiBIncrementsComboBox', h=32, w=50, p='SLiBTweakMenuLayout')
                        cmds.menuItem(l='1', p='SLiBIncrementsComboBox')
                        cmds.menuItem(l='5', p='SLiBIncrementsComboBox')
                        cmds.menuItem(l='45', p='SLiBIncrementsComboBox')
                        cmds.optionMenu('SLiBIncrementsComboBox', e=1, v='45')

                        moveLocJob = cmds.scriptJob(attributeChange=[impObject+'.translate', SLiBAssetTweakJob])
                        rotateLocJob = cmds.scriptJob(attributeChange=[impObject+'.rotate', SLiBAssetTweakJob])
                        scaleLocJob = cmds.scriptJob(attributeChange=[impObject+'.scale', SLiBAssetTweakJob])

                        cmds.rowColumnLayout('SLiBTweakSliderRotLayout', w=264, nc=3, cw=[(1, 200), (2, 32), (2, 32)], p='sliderWinLayout')
                        cmds.floatSliderGrp('AssetRotX', l="Rot X:", cw=[(1, 40),(2, 50)], adj=3, cal=[(1, "left"),(2, "center"),(3, "right")], dc=lambda *args: SLiBAssetTweak(impObject), cc=lambda *args: SLiBAssetTweak(impObject), f=1, min=0, max=360, fmn=-1440, fmx=1440, v=rotX, s=0.001, p='SLiBTweakSliderRotLayout')
                        cmds.iconTextButton('ButtonAssetRotXMinus', w=32, h=32, mw=0, mh=0, c=lambda *args: SLiBTweak('minus', 'AssetRotX', impObject), i=SLiBImage+'slib_minus_square.png', p='SLiBTweakSliderRotLayout')
                        cmds.iconTextButton('ButtonAssetRotXPlus', w=32, h=32, mw=0, mh=0, c=lambda *args: SLiBTweak('plus', 'AssetRotX', impObject), i=SLiBImage+'slib_plus_square.png', p='SLiBTweakSliderRotLayout')
                        cmds.floatSliderGrp('AssetRotY', l="Rot Y:", cw=[(1, 40),(2, 50)], adj=3, cal=[(1, "left"),(2, "center"),(3, "right")], dc=lambda *args: SLiBAssetTweak(impObject), cc=lambda *args: SLiBAssetTweak(impObject), f=1, min=0, max=360, fmn=-1440, fmx=1440, v=rotY, s=0.001, p='SLiBTweakSliderRotLayout')
                        cmds.iconTextButton('ButtonAssetRotYMinus', w=32, h=32, mw=0, mh=0, c=lambda *args: SLiBTweak('minus', 'AssetRotY', impObject),  i=SLiBImage+'slib_minus_square.png', p='SLiBTweakSliderRotLayout')
                        cmds.iconTextButton('ButtonAssetRotYPlus', w=32, h=32, mw=0, mh=0, c=lambda *args: SLiBTweak('plus', 'AssetRotY', impObject),  i=SLiBImage+'slib_plus_square.png', p='SLiBTweakSliderRotLayout')
                        cmds.floatSliderGrp('AssetRotZ', l="Rot Z:", cw=[(1, 40),(2, 50)], adj=3, cal=[(1, "left"),(2, "center"),(3, "right")], dc=lambda *args: SLiBAssetTweak(impObject), cc=lambda *args: SLiBAssetTweak(impObject), f=1, min=0, max=360, fmn=-1440, fmx=1440, v=rotZ, s=0.001, p='SLiBTweakSliderRotLayout')
                        cmds.iconTextButton('ButtonAssetRotZMinus', w=32, h=32, mw=0, mh=0, c=lambda *args: SLiBTweak('minus', 'AssetRotZ', impObject),  i=SLiBImage+'slib_minus_square.png', p='SLiBTweakSliderRotLayout')
                        cmds.iconTextButton('ButtonAssetRotZPlus', w=32, h=32, mw=0, mh=0, c=lambda *args: SLiBTweak('plus', 'AssetRotZ', impObject),  i=SLiBImage+'slib_plus_square.png', p='SLiBTweakSliderRotLayout')

                        cmds.rowColumnLayout('SLiBTweakSliderPosLayout', w=260, nc=3, cw=[(1, 200), (2, 32), (2, 32)], p='sliderWinLayout')
                        cmds.floatSliderGrp('AssetPosX', l="Pos X:", cw=[(1, 40),(2, 50)], adj=3, cal=[(1, "left"),(2, "center"),(3, "right")], dc=lambda *args: SLiBAssetTweak(impObject), cc=lambda *args: SLiBAssetTweak(impObject), f=1, min=-100, max=100, fmn=-3000, fmx=3000, v=posX, s=0.001, p='SLiBTweakSliderPosLayout')
                        cmds.iconTextButton('ButtonAssetPosXMinus', w=32, h=32, mw=0, mh=0, c=lambda *args: SLiBTweak('minus', 'AssetPosX', impObject), i=SLiBImage+'slib_minus_square.png', p='SLiBTweakSliderPosLayout')
                        cmds.iconTextButton('ButtonAssetPosXPlus', w=32, h=32, mw=0, mh=0, c=lambda *args: SLiBTweak('plus', 'AssetPosX', impObject), i=SLiBImage+'slib_plus_square.png', p='SLiBTweakSliderPosLayout')
                        cmds.floatSliderGrp('AssetPosY', l="Pos Y:", cw=[(1, 40),(2, 50)], adj=3, cal=[(1, "left"),(2, "center"),(3, "right")], dc=lambda *args: SLiBAssetTweak(impObject), cc=lambda *args: SLiBAssetTweak(impObject), f=1, min=-100, max=100, fmn=-3000, fmx=3000, v=posY, s=0.001, p='SLiBTweakSliderPosLayout')
                        cmds.iconTextButton('ButtonAssetPosYMinus', w=32, h=32, mw=0, mh=0, c=lambda *args: SLiBTweak('minus', 'AssetPosY', impObject), i=SLiBImage+'slib_minus_square.png', p='SLiBTweakSliderPosLayout')
                        cmds.iconTextButton('ButtonAssetPosYPlus', w=32, h=32, mw=0, mh=0, c=lambda *args: SLiBTweak('plus', 'AssetPosY', impObject), i=SLiBImage+'slib_plus_square.png', p='SLiBTweakSliderPosLayout')
                        cmds.floatSliderGrp('AssetPosZ', l="Pos Z:", cw=[(1, 40),(2, 50)], adj=3, cal=[(1, "left"),(2, "center"),(3, "right")], dc=lambda *args: SLiBAssetTweak(impObject), cc=lambda *args: SLiBAssetTweak(impObject), f=1, min=-100, max=100, fmn=-3000, fmx=3000, v=posZ, s=0.001, p='SLiBTweakSliderPosLayout')
                        cmds.iconTextButton('ButtonAssetPosZMinus', w=32, h=32, mw=0, mh=0, c=lambda *args: SLiBTweak('minus', 'AssetPosZ', impObject), i=SLiBImage+'slib_minus_square.png', p='SLiBTweakSliderPosLayout')
                        cmds.iconTextButton('ButtonAssetPosZPlus', w=32, h=32, mw=0, mh=0, c=lambda *args: SLiBTweak('plus', 'AssetPosZ', impObject), i=SLiBImage+'slib_plus_square.png', p='SLiBTweakSliderPosLayout')

                        cmds.rowColumnLayout('SLiBTweakSliderSclLayout',  w=260,nc=3, cw=[(1, 200), (2, 32), (2, 32)], p='sliderWinLayout')
                        cmds.floatSliderGrp('AssetScaleX', l="Scl X:", cw=[(1, 40),(2, 50)], adj=3, cal=[(1, "left"),(2, "center"),(3, "right")], dc=lambda *args: SLiBAssetTweak(impObject), cc=lambda *args: SLiBAssetTweak(impObject), f=1, min=0.1, max=10, fmn=-3000, fmx=3000, v=scalX, s=0.001, p='SLiBTweakSliderSclLayout')
                        cmds.iconTextButton('ButtonAssetScaleXMinus', w=32, h=32, mw=0, mh=0, c=lambda *args: SLiBTweak('minus', 'AssetScaleX', impObject), i=SLiBImage+'slib_minus_square.png', p='SLiBTweakSliderSclLayout')
                        cmds.iconTextButton('ButtonAssetScaleXPlus', w=32, h=32, mw=0, mh=0, c=lambda *args: SLiBTweak('plus', 'AssetScaleX', impObject), i=SLiBImage+'slib_plus_square.png', p='SLiBTweakSliderSclLayout')
                        cmds.floatSliderGrp('AssetScaleY', l="Scl Y:", cw=[(1, 40),(2, 50)], adj=3, cal=[(1, "left"),(2, "center"),(3, "right")], dc=lambda *args: SLiBAssetTweak(impObject), cc=lambda *args: SLiBAssetTweak(impObject), f=1, min=0.1, max=10, fmn=-3000, fmx=3000, v=scalY, s=0.001, p='SLiBTweakSliderSclLayout')
                        cmds.iconTextButton('ButtonAssetScaleYMinus', w=32, h=32, mw=0, mh=0, c=lambda *args: SLiBTweak('minus', 'AssetScaleY', impObject), i=SLiBImage+'slib_minus_square.png', p='SLiBTweakSliderSclLayout')
                        cmds.iconTextButton('ButtonAssetScaleYPlus', w=32, h=32, mw=0, mh=0, c=lambda *args: SLiBTweak('plus', 'AssetScaleY', impObject), i=SLiBImage+'slib_plus_square.png', p='SLiBTweakSliderSclLayout')
                        cmds.floatSliderGrp('AssetScaleZ', l="Scl Z:", cw=[(1, 40),(2, 50)], adj=3, cal=[(1, "left"),(2, "center"),(3, "right")], dc=lambda *args: SLiBAssetTweak(impObject), cc=lambda *args: SLiBAssetTweak(impObject), f=1, min=0.1, max=10, fmn=-3000, fmx=3000, v=scalZ, s=0.001, p='SLiBTweakSliderSclLayout')
                        cmds.iconTextButton('ButtonAssetScaleZMinus', w=32, h=32, mw=0, mh=0, c=lambda *args: SLiBTweak('minus', 'AssetScaleZ', impObject), i=SLiBImage+'slib_minus_square.png', p='SLiBTweakSliderSclLayout')
                        cmds.iconTextButton('ButtonAssetScaleZPlus', w=32, h=32, mw=0, mh=0, c=lambda *args: SLiBTweak('plus', 'AssetScaleZ', impObject), i=SLiBImage+'slib_plus_square.png', p='SLiBTweakSliderSclLayout')
                        cmds.popupMenu(parent="SLiBTweakSliderSclLayout", ctl=0, button=3)
                        cmds.menuItem('UniformScale', l='Uniform Scale ON', c=lambda *args: SLiBTweakUniform(impObject), ann='off')
                        cmds.window('SLiBTweakWin', e=1, w=264, h=200)

                        sliderWin = wrapInstance(long(omui.MQtUtil.findControl('SLiBTweakWin')), QtGui.QWidget)
                        sliderWin.setWindowFlags(QtCore.Qt.Tool|QtCore.Qt.WA_MouseTracking)
                        sliderWin.setWindowFlags(QtCore.Qt.Tool|QtCore.Qt.WA_TranslucentBackground)
                        sliderWin.setMouseTracking(True)
                        sliderWin.setWindowFlags(QtCore.Qt.Tool|QtCore.Qt.FramelessWindowHint)
                        sliderWin.setWindowOpacity(0.75)
                        sliderWin.show()
                        pos = QtGui.QCursor.pos()
                        sliderWin.move(pos.x(), pos.y())

            except:
                if cmds.objExists('hitLoc'):
                    cmds.delete('hitLoc')
                if cmds.objExists('Placer'):
                    cmds.delete('Placer')
                if cmds.objExists('targetLoc'):
                    cmds.delete('targetLoc')
                try:
                    if cmds.objExists(impObject):
                        cmds.delete(impObject)
                except:
                    pass
                #cmds.select(hitObject)
                SLiBMessager('Error!  Try again or restart IPT', 'red')

    cmds.draggerContext(Context, pressCommand=onPress, dragCommand=onDrag, releaseCommand=onRelease, name=Context, cursor='crossHair')
    cmds.setToolTo(Context)

def SLiBIPTWinClose():
    if cmds.window('SLiBTweakWin', q=1, ex=1):
        cmds.deleteUI('SLiBTweakWin')

def getDAGObject(dagstring):
    sList = om.MSelectionList()
    meshDP = om.MDagPath()
    sList.add(dagstring)
    sList.getDagPath(0,meshDP)

    return meshDP;

def viewToWorld(x,y):
    currentView = omui.M3dView().active3dView()
    resultPt = om.MPoint()
    resultVtr = om.MVector()
    currentView.viewToWorld(int(x),int(y),resultPt,resultVtr)
    return [resultPt.x,resultPt.y,resultPt.z],[resultVtr.x,resultVtr.y,resultVtr.z]

def intersect(dag, pos, dir, farclip):
    targetDAGPath = getDAGObject(dag)
    meshObj = om.MFnMesh(targetDAGPath)
    posFP = om.MFloatPoint(pos[0],pos[1],pos[2])
    dirFP = om.MFloatVector(dir[0],dir[1],dir[2])

    hitFPoint = om.MFloatPoint()
    hitFace = om.MScriptUtil()
    hitTri = om.MScriptUtil()
    hitFace.createFromInt(0)
    hitTri.createFromInt(0)

    hFacePtr = hitFace.asIntPtr()
    hTriPtr = hitTri.asIntPtr()

    hit = meshObj.closestIntersection( posFP,
                                dirFP,
                                None,
                                None,
                                True,
                                om.MSpace.kWorld,
                                farclip,
                                True,
                                None,
                                hitFPoint,
                                None,
                                hFacePtr,
                                hTriPtr,
                                None,
                                None)

    return hit, hitFPoint, meshObj, hitFace.getInt(hFacePtr), hitTri.getInt(hTriPtr)

def crossProduct(vA,vB):
    vC = om.MVector(((vA.y*vB.z) - (vA.z*vB.y)) , ((vA.z*vB.x) - (vA.x*vB.z)), ((vA.x*vB.y) - (vA.y*vB.x)))
    vC.normalize()
    return vC

def dotProduct(vA,vB):
    result = (vA.x * vB.x) + (vA.y * vB.y) + (vA.z * vB.z)
    return result

def vectorAngle(vA,vB):
    scalar = dotProduct(vA,vB)
    mag = magnitude(vA) + magnitude(vB)
    angle = math.acos(scalar/mag)
    return angle

def magnitude(v):
    magnitude = math.sqrt( math.pow(v.x,2) + math.pow(v.y,2) + math.pow(v.z,2) )
    return magnitude
    print magnitude

def getEulerRotationQuaternion(upvector, directionvector):
    quat = om.MQuaternion(upvector, directionvector)
    quatAsEuler = om.MEulerRotation()
    quatAsEuler = quat.asEulerRotation()
    return math.degrees(quatAsEuler.x), math.degrees(quatAsEuler.y), math.degrees(quatAsEuler.z)

def SLiBAssetTweakSliderOn():
    cmds.floatSliderGrp('AssetRotX', e=1, en=1)
    cmds.floatSliderGrp('AssetRotY', e=1, en=1)
    cmds.floatSliderGrp('AssetRotZ', e=1, en=1)
    cmds.floatSliderGrp('AssetPosX', e=1, en=1)
    cmds.floatSliderGrp('AssetPosY', e=1, en=1)
    cmds.floatSliderGrp('AssetPosZ', e=1, en=1)
    cmds.floatSliderGrp('AssetScaleX', e=1, en=1)
    cmds.floatSliderGrp('AssetScaleY', e=1, en=1)
    cmds.floatSliderGrp('AssetScaleZ', e=1, en=1)

def SLiBAssetTweakSliderOff():
    cmds.floatSliderGrp('AssetRotX', e=1, en=0)
    cmds.floatSliderGrp('AssetRotY', e=1, en=0)
    cmds.floatSliderGrp('AssetRotZ', e=1, en=0)
    cmds.floatSliderGrp('AssetPosX', e=1, en=0)
    cmds.floatSliderGrp('AssetPosY', e=1, en=0)
    cmds.floatSliderGrp('AssetPosZ', e=1, en=0)
    cmds.floatSliderGrp('AssetScaleX', e=1, en=0)
    cmds.floatSliderGrp('AssetScaleY', e=1, en=0)
    cmds.floatSliderGrp('AssetScaleZ', e=1, en=0)

def SLiBAssetTweakJob():
    if cmds.iconTextRadioButton('IPTmoveCtx', q=1, sl=1) or cmds.iconTextRadioButton('IPTrotateCtx', q=1, sl=1) or cmds.iconTextRadioButton('IPTscaleCtx', q=1, sl=1) == True:

        jobObject = cmds.ls(sl=1)

        TweakRotX = cmds.getAttr(jobObject[0]+ ".rotateX")
        TweakRotY = cmds.getAttr(jobObject[0]+ ".rotateY")
        TweakRotZ = cmds.getAttr(jobObject[0]+ ".rotateZ")
        TweakPosX = cmds.getAttr(jobObject[0]+ ".translateX")
        TweakPosY = cmds.getAttr(jobObject[0]+ ".translateY")
        TweakPosZ = cmds.getAttr(jobObject[0]+ ".translateZ")
        TweakScaleX = cmds.getAttr(jobObject[0]+ ".scaleX")
        TweakScaleY = cmds.getAttr(jobObject[0]+ ".scaleY")
        TweakScaleZ = cmds.getAttr(jobObject[0]+ ".scaleZ")

        cmds.floatSliderGrp('AssetRotX', e=1, v=TweakRotX)
        cmds.floatSliderGrp('AssetRotY', e=1, v=TweakRotY)
        cmds.floatSliderGrp('AssetRotZ', e=1, v=TweakRotZ)
        cmds.floatSliderGrp('AssetPosX', e=1, v=TweakPosX)
        cmds.floatSliderGrp('AssetPosY', e=1, v=TweakPosY)
        cmds.floatSliderGrp('AssetPosZ', e=1, v=TweakPosZ)
        cmds.floatSliderGrp('AssetScaleX', e=1, v=TweakScaleX)
        cmds.floatSliderGrp('AssetScaleY', e=1, v=TweakScaleY)
        cmds.floatSliderGrp('AssetScaleZ', e=1, v=TweakScaleZ)

def SLiBAssetTweak(impObject):
    TweakRotX = float(cmds.floatSliderGrp('AssetRotX', q=1, v=1))
    TweakRotY = float(cmds.floatSliderGrp('AssetRotY', q=1, v=1))
    TweakRotZ = float(cmds.floatSliderGrp('AssetRotZ', q=1, v=1))
    cmds.setAttr(impObject+ ".rotateX", TweakRotX)
    cmds.setAttr(impObject+ ".rotateY", TweakRotY)
    cmds.setAttr(impObject+ ".rotateZ", TweakRotZ)

    TweakPosX = float(cmds.floatSliderGrp('AssetPosX', q=1, v=1))
    TweakPosY = float(cmds.floatSliderGrp('AssetPosY', q=1, v=1))
    TweakPosZ = float(cmds.floatSliderGrp('AssetPosZ', q=1, v=1))
    cmds.setAttr(impObject+ ".translateX", TweakPosX)
    cmds.setAttr(impObject+ ".translateY", TweakPosY)
    cmds.setAttr(impObject+ ".translateZ", TweakPosZ)

    TweakScaleX = float(cmds.floatSliderGrp('AssetScaleX', q=1, v=1))
    TweakScaleY = float(cmds.floatSliderGrp('AssetScaleY', q=1, v=1))
    TweakScaleZ = float(cmds.floatSliderGrp('AssetScaleZ', q=1, v=1))

    if cmds.menuItem('UniformScale', q=1, ann=1) == 'on':
        cmds.setAttr(impObject+ ".scaleX", TweakScaleX)
        cmds.setAttr(impObject+ ".scaleY", TweakScaleX)
        cmds.setAttr(impObject+ ".scaleZ", TweakScaleX)
        cmds.floatSliderGrp('AssetScaleY', e=1, en=0, v=TweakScaleX)
        cmds.floatSliderGrp('AssetScaleZ', e=1, en=0, v=TweakScaleX)

    if cmds.menuItem('UniformScale', q=1, ann=1) == 'off':
        cmds.setAttr(impObject+ ".scaleX", TweakScaleX)
        cmds.setAttr(impObject+ ".scaleY", TweakScaleY)
        cmds.setAttr(impObject+ ".scaleZ", TweakScaleZ)
        cmds.floatSliderGrp('AssetScaleY', e=1, en=1)
        cmds.floatSliderGrp('AssetScaleZ', e=1, en=1)

def SLiBTweakUniform(impObject):
    if cmds.menuItem('UniformScale', q=1, ann=1) == 'off':
        cmds.menuItem('UniformScale', e=1, l='Uniform Scale OFF', ann='on')
    else:
        cmds.menuItem('UniformScale', e=1, l='Uniform Scale ON', ann='off')

    SLiBAssetTweak(impObject)

def SLiBTweak(op, slider, impObject):
    incr = cmds.optionMenu('SLiBIncrementsComboBox', q=1, v=1)
    if incr == '-':
        increment = 0.01
    else:
        increment = int(incr)
    if op == 'plus':
        Tweak = float(cmds.floatSliderGrp(slider, q=1, v=1))
        cmds.floatSliderGrp(slider, e=1, v=Tweak+increment)
    if op == 'minus':
        Tweak = float(cmds.floatSliderGrp(slider, q=1, v=1))
        cmds.floatSliderGrp(slider, e=1, v=Tweak-increment)
    SLiBAssetTweak(impObject)

def SLiBTrashLast(impObject):
    try:
        if cmds.window('SLiBTweakWin', ex=1):
            cmds.deleteUI('SLiBTweakWin')
        cmds.delete(impObject)
        print 'SLiB >>> Last placed asset trashed!\n',
        cmds.setToolTo('iptContext')
    except:
        pass

def SLiBiptOff():
    if cmds.window('SLiBTweakWin', ex=1):
        cmds.deleteUI('SLiBTweakWin')

    cmds.iconTextCheckBox('IPTPlaceBTN', e=1, v=0)
    cmds.iconTextCheckBox('IPTMoveBTN', e=1, v=0)
    cmds.iconTextCheckBox('IPTPaintBTN', e=1, v=0)

    cmds.inViewMessage('IPTinview', cl='topCenter')
    cmds.setToolTo("selectSuperContext")

    if cmds.draggerContext('iptContext', ex=1):
        cmds.deleteUI('iptContext')
    if cmds.objExists('hitLoc'):
        cmds.delete('hitLoc')
    if cmds.objExists('targetLoc'):
        cmds.delete('targetLoc')
    if cmds.objExists('Placer'):
        cmds.delete('Placer')
    if cmds.objExists('placerMAT'):
        cmds.delete('placerMAT')

    #SLiBMessager("IPT stopped!", 'green')
    #cmds.select(hitObject)

def loadTestRoom():
    answer = cmds.confirmDialog( title='Warning', message='Please make sure you saved the current scene! \nDo you want to proceed?', button=['Yes','No'], defaultButton='Yes', cancelButton='No', dismissString='No' )
    if answer == 'Yes':
        needSave = cmds.file(q=1, modified=1)
        testroomfile = gib('library') + '/scene/' + getRenderEngine() + '_SLiB_preview.ma'
        cmds.file( f=1, new=1 )
        cmds.file( testroomfile, o=1 )
        savename = cmds.workspace(q = 1, fullName = 1) + '/' + getRenderEngine() + '_' + cmds.date().replace (" ", "_").replace ("/", "").replace (":", "") + '.ma'
        cmds.file( rename=savename )
        cmds.file( save=1, type='mayaAscii' )
        SLiBMessager('ShaderBall Scene for ' + str(getRenderEngine()) + ' loaded!', 'green')
    else:
        sys.exit()

def SLiBSaveWindowSettings():
    SLIBPrefs = []
    if MAYA_VERSION != '2017':
        docked = cmds.menuItem('dockMenu', q=1, cb=1)
    else:
        docked = 1
    winX = cmds.window('SLiBBrowserUI', q=1, w=1)
    winY = cmds.window('SLiBBrowserUI', q=1, h=1)
    res = str(cmds.textField('SLiBThumbSizeComboBox', q=1, tx=1))
    col = str(cmds.textField('SLiBThumbColumnsComboBox', q=1, tx=1))
    type = str(cmds.iconTextRadioCollection('mainCatCollection', q=1, sl=1))
    impRef = cmds.menuItem('importREF' , q=1, cb=1)
    if cmds.menuItem('exportMA', q=1, rb=1 ) == 1:
        expExt = 'MA'
    if cmds.menuItem('exportMB', q=1, rb=1 ) == 1:
        expExt = 'MB'
    if cmds.menuItem('exportOBJ', q=1, rb=1 ) == 1:
        expExt = 'OBJ'
    expTex = cmds.menuItem('exportTEX', q=1, cb=1)
    expFRZ = cmds.menuItem('exportFRZ', q=1, cb=1)
    expPIV = cmds.menuItem('exportPIV', q=1, cb=1)
    expHIS = cmds.menuItem('exportHIS', q=1, cb=1)
    delConf = cmds.menuItem('QuickDelete', q=1, cb=1)
    reuAsst = cmds.menuItem('ReUseAsset', q=1, cb=1)
    reuDupl = cmds.menuItem('ReUseDuplicate', q=1, rb=1 )
    reuInst =  cmds.menuItem('ReUseInstance', q=1, rb=1 )
    reuShdr =  cmds.menuItem('ReUseShader', q=1, cb=1 )

    SLIBPrefs.append(docked)
    SLIBPrefs.append(winX)
    SLIBPrefs.append(winY)
    SLIBPrefs.append(res)
    SLIBPrefs.append(col)
    SLIBPrefs.append(type)
    SLIBPrefs.append(impRef)
    SLIBPrefs.append(expExt)
    SLIBPrefs.append(expTex)
    SLIBPrefs.append(expFRZ)
    SLIBPrefs.append(expPIV)
    SLIBPrefs.append(expHIS)
    SLIBPrefs.append(delConf)
    SLIBPrefs.append(reuAsst)
    SLIBPrefs.append(reuDupl)
    SLIBPrefs.append(reuInst)
    SLIBPrefs.append(reuShdr)

    f = open(mel.eval('getenv SLiBLib;') + 'settings/' + 'windowPrefs.txt', 'w')
    f.write(str(SLIBPrefs).replace("'",'').replace(' ','').replace('[','').replace(']','').replace('True','1').replace('False','0'))
    f.close()
    SLiBMessager('Window Layout saved!', 'green')

def SLiBLoadWindowSettings():
    f = open(mel.eval('getenv SLiBLib;') + 'settings/' + 'windowPrefs.txt', 'r')
    s = f.read()
    p = s.split(',')

    if MAYA_VERSION != '2017':
        if int(p[0]) == 1:
            cmds.menuItem('dockMenu', e=1, cb=1)
            SLiBBrowserDockedUI()
            cmds.dockControl('slBrowserDock', e=1, w=int(p[1]))

    if int(p[0]) == 0:
        cmds.window('SLiBBrowserUI', e=1, w=int(p[1]), h=int(p[2]))
    cmds.iconTextRadioCollection('mainCatCollection', e=1, sl=p[5])
    cmds.textField('SLiBThumbSizeComboBox', e=1, tx=int(p[3]))
    cmds.textField('SLiBThumbColumnsComboBox', e=1, tx=int(p[4]))
    cmds.menuItem('importREF' , e=1, cb=int(p[6]))
    cmds.menuItem('export'+p[7], e=1, rb=1)
    cmds.menuItem('exportTEX', e=1, cb=int(p[8]))
    cmds.menuItem('exportFRZ', e=1, cb=int(p[9]))
    cmds.menuItem('exportPIV', e=1, cb=int(p[10]))
    cmds.menuItem('exportHIS', e=1, cb=int(p[11]))
    cmds.menuItem('QuickDelete', e=1, cb=int(p[12]))
    cmds.menuItem('ReUseAsset', e=1, cb=int(p[13]))
    cmds.menuItem('ReUseDuplicate', e=1, rb=int(p[14]))
    cmds.menuItem('ReUseInstance', e=1, rb=int(p[15]))
    cmds.menuItem('ReUseShader', e=1, cb=int(p[16]))

def SLiBBatchTextures():
    assetPath = SLiBCurrLoc()
    destPath = assetPath + '/_THUMBS/'
    failed=[]
    if gib('mainCat') == 'textures':
        if gib('cat') != 'Select...':
            if os.path.isdir(destPath) != True:
                os.mkdir(destPath)
            textureList = os.listdir(assetPath)
            for file in textureList:
                if 'swatch' not in file:
                    if '_THUMBS' not in file:
                        if '_SUB' not in file:
                            if 'mayaSwatches' not in file:
                                cmds.progressBar('PreviewProgress', e=1, max=len(textureList))
                                p = os.path.join(assetPath,file)
                                d = os.path.join(destPath,file)
                                try:
                                    image = om.MImage()
                                    image.readFromFile(p)
                                    image.resize( 512, 512 )
                                    image.writeToFile(d, 'png')
                                    print 'SLIB >>> Thumbnail generated for: ' + file
                                except:
                                    print 'SLIB>>> Failed for: ' + file
                                    failed.append(file)
                                cmds.progressBar('PreviewProgress', e=1, step=1)
            cmds.progressBar('PreviewProgress', e=1, pr=0)
            SLiBBrowserUpdateTextures()
            if len(failed) != 0:
                SLiBMessager('Thumbnail Generation failed for: ' + str(len(failed)) + 'File(s)! Please check Script Editor.', 'red')
            else:
                SLiBMessager('Thumbnail Generation finished!', 'green')
        else:
            SLiBMessager('Please select a Texture Category!', 'red')
            sys.exit()
    else:
        SLiBMessager('Only working with Textures!', 'red')
        sys.exit()

def SLiBMassExport():
    global MassExportUI
    try:
        if cmds.window(MassExportUI, q=1, ex=1):
            cmds.deleteUI(MassExportUI)
    except:
        pass
    MassExportUI = cmds.loadUI(uiFile = SLiBGuiPath + 'SLiBMassExport.ui')
    cmds.showWindow(MassExportUI)
    cmds.textScrollList('listView', p='massList_layout')
    cmds.popupMenu(parent="listView", ctl=0, button=3)
    cmds.menuItem(l='Remove Selected', c=lambda *args: cmds.textScrollList('listView', e=1, ri=cmds.textScrollList('listView', q=1, si=1)))
    cmds.menuItem(l='Clear List', c=lambda *args:  cmds.textScrollList('listView', e=1, ra=1))
    cmds.window(MassExportUI, e=1, te=1)

def SLiBMassExportExit():
    cmds.deleteUI(MassExportUI)

def SLiBMassExportPath():
    massExportPath = cmds.fileDialog2(startingDirectory = os.sep, fileMode = 3)
    if massExportPath != None:
        cmds.textField('SLiB_TEXTFIELD_MassExportPath', e=1, tx = massExportPath[0])
        SLiBMassExportFolder(massExportPath)

def SLiBMassExportFolder(massExportPath):
    massFolder = os.listdir(massExportPath[0])
    global massList
    massList = []
    for files in massFolder:
        file = os.path.splitext(files)[0]
        fileEx = os.path.splitext(files)[1]
        if fileEx == '.mb' or fileEx == '.ma' or fileEx == '.obj' or fileEx == '.obj':
            cmds.textScrollList('listView', e=1, a=files)
            massList.append(files)

def SLiBMassExportFolderProc():
    if gib('cat') != (None or 'Select...'):
        if gib('mainCat') == 'objects':
            massExportPath = cmds.textField('SLiB_TEXTFIELD_MassExportPath', q=1, tx=1)
            cmds.progressBar('massProgressBar', e=1, max=len(massList))
            answer = cmds.confirmDialog( title='Warning', message='Please make sure you saved the current scene! \nDo you want to proceed?', button=['Yes','No'], defaultButton='Yes', cancelButton='No', dismissString='No' )
            if answer == 'Yes':
                #storePath = cmds.workspace(q=1, fn=1) + "/" + 'images/'
                imageName = SLiBTempStore +'/'+ 'icontemp'
                cmds.file( f=1, new=1 )
                for massItem in massList:
                    asset = cmds.file(massExportPath + '/' + massItem, i=1, uns=0, rnn=1, iv=1)
                    root = cmds.ls(asset, assemblies=1)
                    #sceneShapes = cmds.ls(asset, shapes=1)
                    mel.eval('setNamedPanelLayout("Single Perspective View")')
                    cmds.select(root)
                    cmds.viewFit()
                    perspPanel = cmds.getPanel( withl='Persp View')
                    cmds.setFocus(perspPanel)
                    SLiBBrowserPlayBlast()
                    SLiBNameFromSelection()
                    SLiBBrowserExport()
                    cmds.textScrollList('listView', e=1, ri=massItem)
                    cmds.progressBar('massProgressBar', e=1, step=1)
                    cmds.file( f=1, new=1 )
            else:
                sys.exit()

            cmds.progressBar('massProgressBar', e=1, pr=0)
            failed = cmds.textScrollList('listView', q=1, ai=1)
            if failed != None :
                print 'SLiB >>> Export failed for: '
                for f in failed:
                    print f
            else:
                SLiBMessager('Success!!', 'green')

        else:
            SLiBMessager('Only working for Objects!', 'red')
            sys.exit()
    else:
        SLiBMessager('Please select a Category!', 'red')
        sys.exit()

def SLiBFreeze():
    obj = cmds.ls(sl=1)
    if len(obj) != 0:
        cmds.makeIdentity(obj, apply=1, t=1, r=1, s=1, n=0)
        SLiBMessager('Freezed!', 'blue')
    else:
        SLiBMessager('Please select an Object!', 'red')

def SLiBAutoPLacePivot():
    obj = cmds.ls(sl=1)
    if len(obj) != 0:
        cmds.xform(obj, cp=1)
        bbox = cmds.exactWorldBoundingBox()
        bottom = [(bbox[0] + bbox[3])/2, bbox[1], (bbox[2] + bbox[5])/2]
        cmds.xform(obj, piv=bottom, ws=1)
        cmds.move( 0, 0, 0, obj, rpr=1 )
        SLiBMessager("Object moved to Origin and Pivot placed at bottom!", 'blue')
    else:
        SLiBMessager('Please select an Object!', 'red')

def SLiBRepathTextures():
    texlist = cmds.ls(type='file')
    if getRenderEngine() == 'redshift':
        texlistN = cmds.ls(type='RedshiftNormalMap')

    for i in texlist:
        fileName = cmds.getAttr("%s.fileTextureName" %i)
        justName = fileName.split("/")[-1]
        partPath = os.path.dirname(gib('file')) + '/Tex' "/" + justName
        finalPath = '${SLiBLib}' + str(partPath).split('lib')[1]
        cmds.setAttr("%s.fileTextureName" %i, finalPath, type="string")

    if getRenderEngine() == 'redshift':
        for n in texlistN:
            fileNameN = cmds.getAttr("%s.tex0" %n)
            justNameN = fileNameN.split("/")[-1]
            partPathN = os.path.dirname(gib('file')) + '/Tex' "/" + justNameN
            finalPathN = '${SLiBLib}' + str(partPathN).split('lib')[1]
            cmds.setAttr("%s.fileTextureName" %i, finalPathN, type="string")

def SLiBMessager(message, color):
    cmds.textField('SLiB_TEXTFIELD_Message', e=1 , tx=message)
    if color == 'none':
        cmds.textField('SLiB_TEXTFIELD_Message', e=1 , bgc=[0.15,0.15,0.15])  #neutral
    if color == 'green':
        cmds.textField('SLiB_TEXTFIELD_Message', e=1 , bgc=[0,0.9,0])   #success
    if color == 'yellow':
        cmds.textField('SLiB_TEXTFIELD_Message', e=1 , bgc=[1,0.5,0])   #yellow
    if color == 'red':
        cmds.textField('SLiB_TEXTFIELD_Message', e=1 , bgc=[0.9,0,0])   #error
    if color == 'blue':
        cmds.textField('SLiB_TEXTFIELD_Message', e=1 , bgc=[0,0.75,0.99])   #blue

def SLiBAutoColumns():
    autoCols = (cmds.window('SLiBBrowserUI', q=1, w=1) - 256) / int(cmds.textField('SLiBThumbSizeComboBox', q=1, tx=1))
    if int(cmds.textField('SLiBThumbColumnsComboBox', q=1, tx=1)) != autoCols:
        cmds.textField('SLiBThumbColumnsComboBox', e=1, tx=autoCols)
        SLiBBrowserUpdateShader()

def SLiBMultiSel():
    multiSel = []
    for e in cmds.iconTextRadioCollection('slAssetCollection', q=1, cia=1):
        file = e.split('|icon')[-1]
        if cmds.checkBox('cb_' + file, q=1, v=1) == 1:
            multiSel.append(file)
    if len(multiSel) != 0:
        return multiSel
    else:
        SLiBMessager('Please select something', 'red')

def SLiBBrowserMark(m):
    if cmds.iconTextRadioCollection('slAssetCollection', q=1, ex=1) == 1:
        for e in cmds.iconTextRadioCollection('slAssetCollection', q=1, cia=1):
            file = e.split('|icon')[-1]
            if m == 'none':
                cmds.checkBox('cb_' + file, e=1, v=0)
                SLiBcbToggleOff(file)
            if m == 'all':
                cmds.checkBox('cb_' + file, e=1, v=1)
                SLiBcbToggleOn(file)

def SLiBcbToggleOn(file, *args):
    cmds.rowLayout('caption'+file, e=1, bgc=[0,0.75,0.99])

def SLiBcbToggleOff(file, *args):
    cmds.rowLayout('caption'+file, e=1, bgc=[0.1,0.1,0.1])

def SLiBFileType():
    fileType = os.path.splitext(gib('file'))[1]
    if fileType == '.ma':
        fileType = 'mayaAscii'
    if fileType == '.mb':
        fileType = 'mayaBinary'
    if fileType == '.obj':
        fileType = 'OBJ'
    return fileType

def SLiBBrowserSetUpRendercam():
    if cmds.objExists('SLiBRenderCam'):
        cmds.delete('SLiBRenderCam')
    perspPanel = cmds.getPanel( withl='Persp View')
    cmds.setFocus(perspPanel)
    camera1 = cmds.modelPanel(cmds.getPanel(wf=1), q=1, cam=1)
    cmds.duplicate(camera1, n='SLiBRenderCam')
    cmds.lookThru( 'SLiBRenderCam' )

    if getRenderEngine() == 'vray':
        cmds.setAttr('vraySettings.wi',512)
        cmds.setAttr('vraySettings.he', 512)
        cmds.setAttr('vraySettings.aspr', 1)
        cmds.setAttr('vraySettings.pxa', 1)
        cmds.setAttr('vraySettings.vfbOn', 0)
        cmds.setAttr('vraySettings.sRGBOn', 1)
        mel.eval("vray addAttributesFromGroup |SLiBRenderCam|SLiBRenderCamShape vray_cameraPhysical 1")
    else:
        cmds.setAttr('defaultResolution.width', 512)
        cmds.setAttr('defaultResolution.height', 512)
        cmds.setAttr('defaultResolution.deviceAspectRatio', 1)

    cmds.camera('SLiBRenderCam', e=1, displayResolution=1, overscan=3.0)

def SLiBTTWin():
    if cmds.window('TTMainWin', q=1, ex=1):
        cmds.deleteUI('TTMainWin')
    cmds.window('TTMainWin', title="Texture Tools", iconName='Short Name', widthHeight=(700, 250) )
    cmds.columnLayout('TTMainLayout', adj=1 )
    cmds.rowColumnLayout('TTMainButtons', numberOfRows=1, p='TTMainLayout')
    cmds.button(l='Refresh', c=lambda *args: SLiBListTextures(), bgc=[0,0.75,0.99], p='TTMainButtons')
    cmds.text(l='', p='TTMainButtons')
    cmds.button(l='ABS > REL', c=lambda *args: ABStoREL(), bgc=[0,0.75,0.99], p='TTMainButtons')
    cmds.text(l='', p='TTMainButtons')
    cmds.button(l='REL > ABS', c=lambda *args: RELtoABS(), bgc=[0,0.75,0.99], p='TTMainButtons')
    cmds.text(l='   copy and set path(s) to:  ', p='TTMainButtons')
    cmds.button(l=' current project ', c=lambda *args: copyTexturesToProject('cp'), bgc=[0,0.75,0.99], p='TTMainButtons')
    cmds.text(l='  or  ', p='TTMainButtons')
    cmds.button('cf_button', l='', w=200, c=lambda *args: copyTexturesToProject('cf'), en=0, bgc=[0.31,0.31,0.31], p='TTMainButtons')
    cmds.text(l='', p='TTMainButtons')
    cmds.button(l=' Browse ', c=lambda *args: SLiBBrowserNewDest(), p='TTMainButtons')

    cmds.textScrollList('TTScrollField', h=200, w=700, p='TTMainLayout')
    cmds.button(l='Close', command="cmds.deleteUI('TTMainWin')", p='TTMainLayout')
    cmds.window('TTMainWin', e=1, widthHeight=(700, 250))
    cmds.showWindow('TTMainWin')
    SLiBListTextures()

def SLiBBrowserNewDest():
    newDestName = cmds.fileDialog2(startingDirectory = os.sep, fileMode = 3)
    if len(newDestName) != 0:
        cmds.button('cf_button', e=1, en=1, l=newDestName[0], bgc=[0,0.75,0.99])
    else:
        cmds.button('cf_button', e=1, en=0, l='', bgc=[0.31,0.31,0.31])

def SLiBListTextures():
    cmds.textScrollList('TTScrollField', e=1, ra=1)
    texlist = cmds.ls(type='file')
    if getRenderEngine() == 'redshift':
        texlistN = cmds.ls(type='RedshiftNormalMap')
    for i in texlist:
        fileName = cmds.getAttr("%s.fileTextureName" %i)
        cmds.textScrollList('TTScrollField', e=1, a=fileName)

    if getRenderEngine() == 'redshift':
        if texlistN != None or len(texlistN) != 0:
            for n in texlistN:
                fileNameN = cmds.getAttr("%s.tex0" %n)
                cmds.textScrollList('TTScrollField', e=1, a=fileNameN)

def ABStoREL():
    texlist = cmds.ls(type='file')
    if getRenderEngine() == 'redshift':
        texlistN = cmds.ls(type='RedshiftNormalMap')

    for i in texlist:
        if 'maps' in cmds.getAttr("%s.fileTextureName" %i):
            fileName = cmds.getAttr("%s.fileTextureName" %i).split('/maps/')[1]
            fileNameNew = '${SLiBLib}' + '/maps/' + str(fileName)
        else:
            try:
                fileName = cmds.getAttr("%s.fileTextureName" %i).split('/shader/')[1]
                fileNameNew = '${SLiBLib}' + '/shader/' + str(fileName)
            except:
                fileName = cmds.getAttr("%s.fileTextureName" %i).split('/objects/')[1]
                fileNameNew = '${SLiBLib}' + '/objects/' + str(fileName)

        cmds.setAttr("%s.fileTextureName" %i, fileNameNew, type="string")

    if getRenderEngine() == 'redshift':
        if texlistN != None or len(texlistN) != 0:
            for e in texlistN:
                if 'maps' in cmds.getAttr("%s.tex0" %e):
                    fileNameN = cmds.getAttr("%s.tex0" %e).split('/maps/')
                    fileNameNNew = '${SLiBLib}' + '/maps/' + fileNameN[1]

                else:
                    try:
                        fileNameN = cmds.getAttr("%s.tex0" %e).split('/shader/')
                        fileNameNNew = '${SLiBLib}' + '/shader/' + fileNameN[1]
                    except:
                        fileNameN = cmds.getAttr("%s.tex0" %e).split('/objects/')
                        fileNameNNew = '${SLiBLib}' + '/objects/' + fileNameN[1]

                cmds.setAttr("%s.tex0" %e, fileNameNNew, type="string")

    if cmds.window('TTMainWin', q=1, ex=1):
        SLiBListTextures()

def RELtoABS():
    texlist = cmds.ls(type='file')
    if getRenderEngine() == 'redshift':
        texlistN = cmds.ls(type='RedshiftNormalMap')
    for i in texlist:
        fileName = cmds.getAttr("%s.fileTextureName" %i)
        fileNameNew = fileName.replace('${SLiBLib}', mel.eval('getenv SLiBLib;'))
        cmds.setAttr("%s.fileTextureName" %i, fileNameNew, type="string")

    if getRenderEngine() == 'redshift':
        if texlistN != None or len(texlistN) != 0:
            for n in texlistN:
                fileNameN = cmds.getAttr("%s.tex0" %n)
                fileNameNNew = fileNameN.replace('${SLiBLib}', mel.eval('getenv SLiBLib;'))
                cmds.setAttr("%s.tex0" %n, fileNameNNew, type="string")

    if cmds.window('TTMainWin', q=1, ex=1):
        SLiBListTextures()

def copyTexturesToProject(d):
    env = mel.eval('getenv SLiBLib;')
    texlist = cmds.ls(type='file')
    if getRenderEngine() == 'redshift':
        texlistN = cmds.ls(type='RedshiftNormalMap')

    if d == 'cp':
        textdestination = cmds.workspace(q=1, fullName=1) + "/" + 'sourceimages'
        textdestinationN = cmds.workspace(q=1, fullName=1) + "/" + 'sourceimages'
    if d == 'cf':
        textdestination = str(cmds.button('cf_button', q=1, l=1))
        textdestinationN = str(cmds.button('cf_button', q=1, l=1))

    for i in texlist:
        fileName = cmds.getAttr("%s.fileTextureName" %i)
        finalName = fileName.split("}")[-1]
        justName = fileName.split("/")[-1]
        finalPath = textdestination + "/" + justName
        fileOrigin=  env+finalName
        try:
            if os.path.isfile(finalPath) != True:
                shutil.copy(fileOrigin, textdestination)
            cmds.setAttr("%s.fileTextureName" %i, finalPath, type="string")
        except:
            if os.path.isfile(finalPath) != True:
                shutil.copy(finalName, textdestination)
            cmds.setAttr("%s.fileTextureName" %i, finalPath, type="string")

    if getRenderEngine() == 'redshift':
        if texlistN != None or len(texlistN) != 0:
            for n in texlistN:
                fileNameN = cmds.getAttr("%s.tex0" %n)
                finalNameN = fileNameN.split("}")[-1]
                justNameN = fileNameN.split("/")[-1]
                finalPathN = textdestination + "/" + justNameN
                fileOriginN =  env+finalNameN

                try:
                    if os.path.isfile(finalPathN) != True:
                        shutil.copy(fileOriginN, textdestinationN)
                    cmds.setAttr("%s.tex0" %n, finalPathN, type="string")
                except:
                    if os.path.isfile(finalPathN) != True:
                        shutil.copy(finalNameN, textdestinationN)
                    cmds.setAttr("%s.tex0" %n, finalPathN, type="string")

    print '\nDONE! \n\nNew Texture Path set to:\n\n' + textdestination
    if cmds.window('TTMainWin', q=1, ex=1):
        SLiBListTextures()

def SLiBSNRSpeed():
    newShaderName = cmds.textField("SLiB_TEXTFIELD_Name", q=1 ,text=1)
    if len(newShaderName) == 0:
        SLiBMessager('Please give a Name', 'red')
        sys.exit()
    else:
        shaderHolder = cmds.ls(sl=1)
        if len(shaderHolder) != 0:
            if cmds.objectType(shaderHolder) == 'transform' or cmds.objectType(shaderHolder) == 'mesh':
                SG = cmds.listConnections(cmds.ls(sl=1, dag=1, s=1), type="shadingEngine")
                MAT = cmds.ls(cmds.listConnections(SG), mat=1)
                if len(MAT) != 1:
                    for m in MAT:
                        if cmds.nodeType(m) == 'displacementShader':
                            MAT.remove(m)

                cmds.select(MAT)

                snrDict=[]
                f = open( mel.eval('getenv SLiBLib;') +'settings/SNR_Dictionary.txt', 'r')
                for line in f.readlines():
                    snrDict.append([line.rstrip()])

                for i in range(0, len(MAT)):
                    cmds.select(cmds.listHistory(SG), noExpand=1)
                    SNW = cmds.ls(sl=1)
                    for e in SNW:
                        nodeTyp = str(cmds.nodeType(e))
                        nodeTyp_oldList = [x[0].split(',')[0] for x in snrDict]
                        nodeTyp_newList = [x[0].split(',')[1] for x in snrDict]

                        if nodeTyp in nodeTyp_oldList:
                           entspricht = nodeTyp_oldList.index(nodeTyp)
                           cmds.rename(e, newShaderName + nodeTyp_newList[entspricht])
    cmds.select(cl=1)
    cmds.select(shaderHolder)

#########################################################################
#
#  SLiBPreRenderPY.py by DGDM
#
#########################################################################

import maya.cmds as cmds
import maya.mel as mel
import os

def swapMat():
    beforeSG = set(cmds.ls(type="shadingEngine"))
    if os.path.isfile(mel.eval('getenv SLiBLib;') + "/scene/temp_preview_mat.ma"):
        tempMat = mel.eval('getenv SLiBLib;') + "/scene/temp_preview_mat.ma"
    else:
        tempMat = mel.eval('getenv SLiBLib;') + "/scene/temp_preview_mat.mb"
    imported = cmds.file(tempMat, i=1, uns=0, rnn=1, iv=1)
    afterSG  = set(cmds.ls(type="shadingEngine"))
    SG = ', '.join(afterSG.difference(beforeSG))
    cmds.sets('ShaderHolder', e=1, forceElement= SG)

#########################################################################
#
#  SLiBSetupPy.py v0.3 by DGDM
#
#########################################################################


import maya.cmds as cmds
import maya.mel as mel

SLiBImage = mel.eval('getenv SLiBImage;')
SLiBGuiPath = mel.eval('getenv SLiBGui;')
MAYA_VERSION = cmds.about(version=1)

def SLiBSetupMenu():
    gMainWindow = mel.eval('$temp1=$gMainWindow')
    if cmds.menu('SLiBMenu', q=1, ex=1):
        cmds.deleteUI('SLiBMenu', menu = 1)
    SLiBMenu = cmds.menu('SLiBMenu', parent = gMainWindow, tearOff = 1, l='SLiB')

    if MAYA_VERSION != '2017':
        cmds.menuItem(parent = 'SLiBMenu', l = 'SLiB Browser ...', c='import SLiBBrowserPy;reload(SLiBBrowserPy);SLiBBrowserPy.SLiBBrowserUI()')
    else:
        cmds.menuItem(parent = 'SLiBMenu', l = 'SLiB Browser ...', c='import SLiBBrowserPy;reload(SLiBBrowserPy);SLiBBrowserPy.SLiBBrowserWorkspaceControl()')
    
    cmds.menuItem(parent = 'SLiBMenu', d=1)
    cmds.menuItem(parent = 'SLiBMenu', l = 'SLiB FloorGen ...', c='import SLiBFloorGenPY;reload(SLiBFloorGenPY)')
    cmds.menuItem(parent = 'SLiBMenu', d=1) 
    if MAYA_VERSION != '2017':
        cmds.menuItem(parent = 'SLiBMenu', l = 'SLiB Leuchtkraft ...', c='import SLiBLeuchtkraftPY;reload(SLiBLeuchtkraftPY);SLiBLeuchtkraftPY.SLiBLeuchtkraftUI()')
    else:
        cmds.menuItem(parent = 'SLiBMenu', l = 'SLiB Leuchtkraft ...', c='import SLiBLeuchtkraftPY;reload(SLiBLeuchtkraftPY);SLiBLeuchtkraftPY.SLiBLeuchtkraftWorkspaceControl()')
    cmds.menuItem(parent = 'SLiBMenu', d=1) 
    cmds.menuItem(parent = 'SLiBMenu', l = 'SLiB Partikel ...', c='import SLiBPartikelPY;reload(SLiBPartikelPY);SLiBPartikelPY.partikel()')
    cmds.menuItem(parent = 'SLiBMenu', d=1)
    cmds.menuItem(parent = 'SLiBMenu', l = 'SLiB VPR ...', c='import SLiB_VPR;reload(SLiB_VPR);SLiB_VPR.VPRStart()')
    cmds.menuItem(parent = 'SLiBMenu', d=1) 
    cmds.menuItem(parent = 'SLiBMenu', l = 'Homepage', c='import maya;maya.cmds.showHelp("http://store.cgfront.com", absolute=1)')

def SLiBSetupMenuRemove():
    if cmds.menu('SLiBMenu', q=1, ex=1):
        cmds.deleteUI('SLiBMenu', menu = 1)
 
def SLiBSetupLoad():
    SLiBSetupMenu()
    SLiBshelf()

def SLiBSetupUnLoad():
    SLiBSetupMenuRemove()
    if cmds.shelfLayout("SLiB", ex=1):
        cmds.deleteUI("SLiB")
    
def SLiBshelf():
    if cmds.shelfLayout("SLiB", ex=1):
        cmds.deleteUI("SLiB")
    
    shelfTab = mel.eval('global string $gShelfTopLevel;')
    mel.eval('global string $scriptsShelf;')
    mel.eval('$scriptsShelf = `shelfLayout -cellWidth 33 -cellHeight 33 -p $gShelfTopLevel SLiB`;')
    
    if MAYA_VERSION != '2017':
        cmds.shelfButton('browser', i=SLiBImage+'shelf_browser.png', c='import SLiBBrowserPy; reload(SLiBBrowserPy); SLiBBrowserPy.SLiBBrowserUI()', p='SLiB')
    else:
        cmds.shelfButton('browser', i=SLiBImage+'shelf_browser.png', c='import SLiBBrowserPy; reload(SLiBBrowserPy); SLiBBrowserPy.SLiBBrowserWorkspaceControl()', p='SLiB')
    
    if MAYA_VERSION != '2017':
        cmds.shelfButton('leuchtkraft', i=SLiBImage+'shelf_leuchtkraft.png', c='import SLiBLeuchtkraftPY; reload(SLiBLeuchtkraftPY); SLiBLeuchtkraftPY.SLiBLeuchtkraftUI()', p='SLiB')
    else:
        cmds.shelfButton('leuchtkraft', i=SLiBImage+'shelf_leuchtkraft.png', c='import SLiBLeuchtkraftPY; reload(SLiBLeuchtkraftPY); SLiBLeuchtkraftPY.SLiBLeuchtkraftWorkspaceControl()', p='SLiB')
        
    cmds.shelfButton('floorgen', i=SLiBImage+'shelf_floorgen.png', c='import SLiBFloorGenPY; reload(SLiBFloorGenPY)', p='SLiB')
    cmds.shelfButton('partikel', i=SLiBImage+'shelf_partikel.png', c='import SLiBPartikelPY; reload(SLiBPartikelPY); SLiBPartikelPY.partikel()', p='SLiB')
    cmds.shelfButton('vpr', i=SLiBImage+'shelf_vpr.png', c='import SLiB_VPR; reload(SLiB_VPR); SLiB_VPR.VPRStart()', p='SLiB')